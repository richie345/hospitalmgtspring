package com.geemla.hospitalapp.config;

import com.geemla.hospitalapp.constants.BackOfficeType;
import com.geemla.hospitalapp.constants.RoleType;
import com.geemla.hospitalapp.domains.actors.BackofficeUser;
import com.geemla.hospitalapp.domains.actors.User;
import com.geemla.hospitalapp.repositories.actors.BackofficeUserRepository;
import com.geemla.hospitalapp.repositories.actors.UserRepository;
import com.geemla.hospitalapp.repositories.security.PermissionRepository;
import com.geemla.hospitalapp.repositories.security.RoleRepository;
import com.geemla.hospitalapp.security.Permission;
import com.geemla.hospitalapp.security.Role;
import com.geemla.hospitalapp.utils.Accessor;
import com.geemla.hospitalapp.utils.Filter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class ApplicationInitializer implements ApplicationListener<ContextRefreshedEvent> {
    Logger logger = LoggerFactory.getLogger(ApplicationInitializer.class);

    private boolean alreadySetup = false;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BackofficeUserRepository backofficeUserRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PermissionRepository permissionRepository;


    @Autowired
    public ApplicationInitializer(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {

        if (Accessor.count(User.class, Filter.get()) != 0) {
            return;
        }

        // == create initial permissions
        final Permission addPatientPermission = createPermissionIfNotFound("CAN_ADD_PATIENT", "Add Patient");
        final Permission viewPatientPermission = createPermissionIfNotFound("CAN_VIEW_PATIENT", "View Patient");
        final Permission addVitalSignsPermission = createPermissionIfNotFound("CAN_ADD_VITAL_SIGNS", "Add Vital signs");
        final Permission viewVitalSignsPermission = createPermissionIfNotFound("CAN_VIEW_VITAL_SIGNS", "View Vital signs");
        final Permission addDiagnosisPermission = createPermissionIfNotFound("CAN_ADD_DIAGNOSIS", "Add Diagnosis");
        final Permission viewDiagnosisPermission = createPermissionIfNotFound("CAN_VIEW_DIAGNOSIS", "View Diagnosis");
        final Permission addRolePermission = createPermissionIfNotFound("CAN_ADD_ROLE", "Add Role");
        final Permission viewRolePermission = createPermissionIfNotFound("CAN_VIEW_ROLE", "View Role");
        final Permission addUserPermission = createPermissionIfNotFound("CAN_ADD_USER", "Add User");
        final Permission viewUserPermission = createPermissionIfNotFound("CAN_VIEW_USER", "View User");
        final Permission makeSchedulePermission = createPermissionIfNotFound("CAN_MAKE_SCHEDULE", "Make Schedule");
        final Permission viewSchedulePermission = createPermissionIfNotFound("CAN_VIEW_SCHEDULE", "View Schedule");
        final Permission viewAllPatientsPermission = createPermissionIfNotFound("CAN_VIEW_ALL_PATIENTS", "View All Patients");
        final Permission changeProfilePermission = createPermissionIfNotFound("CAN_CHANGE_PROFILE", "Change Profile");

        // == create initial roles Set<String> set = new HashSet<String>(list);
        final Set<Permission> adminPrivileges = new HashSet(Arrays.asList(
                addPatientPermission, viewPatientPermission, addVitalSignsPermission, viewVitalSignsPermission,
                makeSchedulePermission, viewSchedulePermission, addRolePermission, viewRolePermission, addUserPermission,
                viewUserPermission, viewAllPatientsPermission, viewAllPatientsPermission, changeProfilePermission)
        );

        final Set<Permission> doctorPrivileges = new HashSet(Arrays.asList(
                addDiagnosisPermission, viewDiagnosisPermission, viewVitalSignsPermission, viewSchedulePermission,
                viewPatientPermission, changeProfilePermission)
        );


        createRoleIfNotFound("ADMINISTRATOR", "Admin User", adminPrivileges, RoleType.BACKOFFICE);
        createRoleIfNotFound("DOCTOR", "Doctor", doctorPrivileges, RoleType.DOCTOR);

        // == create default Admin user
        createAdminIfNotFound();

    }

    private final Permission createPermissionIfNotFound(final String name, final String description) {
        Permission permission = permissionRepository.findByName(name);
        if (permission == null) {
            permission = new Permission(name, description);
            permissionRepository.save(permission);
        }
        return permission;
    }

    private final Role createRoleIfNotFound(final String name, final String description,
                                            final Set<Permission> permissions, final RoleType roleType) {
        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name, description, permissions, roleType);
            roleRepository.save(role);
        }
        return role;
    }

    @Transactional
    public void createAdminIfNotFound() {
        final Role adminRole = roleRepository.findByName("ADMINISTRATOR");
        if (adminRole != null) {

            User user = new User("Rich", "Admin", "richard.emoche@gmail.com", "test", true);
            user.setRoles(new HashSet<>(Arrays.asList(adminRole)));
            user = userRepository.save(user);

            BackofficeUser backofficeUser = new BackofficeUser();
            backofficeUser.setUser(user);
            backofficeUser.setActive(true);
            backofficeUser.setBackOfficeType(BackOfficeType.ADMIN);

            try {
                backofficeUserRepository.save(backofficeUser);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


}
