package com.geemla.hospitalapp.config;


import com.geemla.hospitalapp.security.*;
import com.geemla.hospitalapp.services.actors.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ConditionalOnEnabledResourceChain;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.ShallowEtagHeaderFilter;
import org.springframework.web.servlet.resource.ResourceUrlEncodingFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings("SpringJavaAutowiringInspection")
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    private final UserDetailsService userDetailsService;

    public WebSecurityConfig(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Bean
    @Primary
    public UserDetailsService getUserDetailsService(UserServiceImpl userService) {
        return userService;
    }

    @Bean
    @ConditionalOnEnabledResourceChain
    public ResourceUrlEncodingFilter resourceUrlEncodingFilter() {
        return new ResourceUrlEncodingFilter();
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean () {
        ShallowEtagHeaderFilter eTagFilter = new ShallowEtagHeaderFilter();
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(eTagFilter);
        registration.addUrlPatterns("/css/**","/js/**","/img/**");
        return registration;
    }

    /*@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }*/

    @Bean
    public static BCryptPasswordEncoder bcryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(bcryptPasswordEncoder());
    }

    @Configuration
    @Order(1)
    public static class ApiConfigurationAdapter extends WebSecurityConfigurerAdapter {

        @Autowired
        private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
        @Autowired
        private TokenProvider tokenProvider;

        @Bean
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }

        @Bean
        public JwtCorsFilter jwtCorsFilter() throws Exception {
            return new JwtCorsFilter();
        }

        @Bean
        public JwtAuthenticationTokenFilter authenticationTokenFilterBean()
        {
            return new JwtAuthenticationTokenFilter(tokenProvider);
        }

        private final String[] API_AUTH_WHITELIST = {
                "/auth/**", "/api/v1/account/resetPassword", "/api/v1/account/registrationConfirm",
                "/api/v1/account/saveResetPassword", "/api/v1/thirdparty/qgen/webhook-callback",
                "/api/v1/thirdparty/stripe/direct-debit/webhook",
                "/swagger-resources/**", "/swagger-ui.html", "/v2/api-docs", "/webjars/**",
                "/resources/**", "/static/**", "/public/**", "/webui/**", "/assets/**", "/",
                "/*.html", "/favicon.ico", "/**/*.html", "/**/*.css", "/**/*.js", "/**/*.png","/**/*.jpg",
                "/**/*.gif", "/**/*.svg", "/**/*.ico", "/**/*.ttf","/**/*.woff", "/**/*/.mp3"
        };

        @Override
        protected void configure(HttpSecurity http) throws Exception {

            http.antMatcher("/api/**").csrf().disable().exceptionHandling()
                    .authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
                    .antMatchers(API_AUTH_WHITELIST).permitAll();

            http.addFilterBefore(jwtCorsFilter(), UsernamePasswordAuthenticationFilter.class)
                    .addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);

            http.headers().cacheControl();
        }

    }

    @Configuration
    @Order(2)
    public static class WebConfigurationAdapter extends WebSecurityConfigurerAdapter {

        private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

        @Autowired
        CustomLogoutSuccessHandler customLogoutSuccessHandler;

        @Bean
        public SessionRegistry sessionRegistry() {
            return new SessionRegistryImpl();
        }

        @Bean
        CustomLoginSuccessHandler authenticationSuccessHandler() {
            return new CustomLoginSuccessHandler();
        }

        @Bean
        FailureLoginHandler authenticationFailureHandler() {
            return new FailureLoginHandler();
        }

        private final String[] WEB_AUTH_WHITELIST = {"/", "/account/forgotpassword",
                "/account/resetpassword","/account/save_reset_password","/account/confirmation", "/actuator/health",
                "/account/registration_confirmation", "/account/login", "/account/register", "/account/resendtoken",
                "/miscellaneous/registration-data/*",
                "/media/**", "/docs/**","/static/media/**", "**/*.ico", "**/*.png",
                "**/*.jpg", "**/*.png", "/*.ico", "/*.json","/assets/images/**", "/error", "401", "/403", "/404", "500",
                "/assets/**", "/ajax/**", "/css/**", "data/**", "/fonts/**", "/img/**", "/js/**", "/php/**", "/sound/**", "xml/**",
                "/v2/api-docs",           // swagger
                "/webjars/**",            // swagger-ui webjars
                "/swagger-resources/**",  // swagger-ui resources
                "/configuration/**",      // swagger configuration
                "/docs",
                "/swagger-ui.html/**",
                "/*.html"
        };

        protected void configure(HttpSecurity http) throws Exception {

            http.authorizeRequests().antMatchers(WEB_AUTH_WHITELIST).permitAll()
                    .anyRequest().authenticated().and()
                    .csrf().disable()
                    .formLogin()
                    .loginPage("/account/login")
                    .successHandler(authenticationSuccessHandler())
                    .failureHandler(authenticationFailureHandler())
                    .usernameParameter("email").passwordParameter("password")
                    .permitAll().and()
                    .rememberMe().and()
                    .logout().logoutSuccessHandler(customLogoutSuccessHandler)
                    .permitAll()
                    .deleteCookies("JSESSIONID")
                    .invalidateHttpSession(true).and().headers().cacheControl();

            http.exceptionHandling().authenticationEntryPoint(new AuthenticationEntryPoint() {
                @Override
                public void commence(HttpServletRequest request,
                                     HttpServletResponse response, AuthenticationException authException)
                        throws IOException, ServletException {

                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);


                    String errorMessage = "You must be logged in to access that page.";

                    redirectStrategy.sendRedirect(request, response,"/account/login?error=" + errorMessage);
                }
            });

            http.sessionManagement()
                    .maximumSessions(-1)
                    .sessionRegistry(sessionRegistry());
        }

        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers("/css/**", "/js/**");
        }
    }
}
