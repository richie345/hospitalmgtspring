package com.geemla.hospitalapp.security;

import com.geemla.hospitalapp.domains.actors.User;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author Emoche
 **/

public final class CustomUserFactory {

    private CustomUserFactory() {
    }

    public static CustomUser create(User user, Collection<? extends GrantedAuthority> authorities) {
        return new CustomUser(
                user.getId(),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getPassword(),
                authorities,
                user.isEnabled(),
                user.getLastPasswordResetDate(),
                user.getLastLoggedIn()
        );
    }
}
