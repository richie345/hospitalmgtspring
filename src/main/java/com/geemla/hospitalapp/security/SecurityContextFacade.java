package com.geemla.hospitalapp.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;

public interface SecurityContextFacade {

    SecurityContext getContext();

    void setContext(SecurityContext securityContext);

    default Authentication getAuthentication() {
        return this.getContext().getAuthentication();
    }

    default String getPrincipal() {
        return this.getAuthentication() != null ? this.getAuthentication().getName() : null;
    }
}
