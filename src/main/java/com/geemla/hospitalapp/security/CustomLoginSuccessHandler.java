package com.geemla.hospitalapp.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class CustomLoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    public CustomLoginSuccessHandler() {
        super();
        LOGGER.info("At CustomLoginSuccessHandler");
        setAlwaysUseDefaultTargetUrl(false);

        String defaultTargetUrl = "/dashboard";
        setDefaultTargetUrl(defaultTargetUrl);
    }
}
