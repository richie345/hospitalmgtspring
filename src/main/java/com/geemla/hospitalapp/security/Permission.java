package com.geemla.hospitalapp.security;

import com.geemla.hospitalapp.domains.base.SimpleModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.util.Objects;

/**
 * @author Emoche
 **/

@Getter
@Setter
@Entity
@Table(name = "permissions")
@NoArgsConstructor
public class Permission extends SimpleModel {

    @Lob
    @Column(name = "description")
    private String description;

    private String name;

    public Permission(String name){
        this.setName(name);
    }

    public Permission(String name, String description){
        this.setName(name);
        this.setDescription(description);
        super.setActive(true);
    }

    public Permission(Long id){
        this.setId(id);
    }

    public void setName(String name) {
        this.name = name.startsWith("ROLE_") ? name : "ROLE_" + name;
        this.name = this.name.trim().toUpperCase();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Permission)) return false;
        Permission that = (Permission) o;
        return this == o
                || Objects.equals(this.getId(), that.getId())
                || (Objects.equals(active, that.active) && Objects.equals(getName(), that.getName()));
    }



    @Override
    public int hashCode() {
        return Objects.hash(getName(), active);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "name:" + this.getName() + "id:" + this.getId();
    }

}
