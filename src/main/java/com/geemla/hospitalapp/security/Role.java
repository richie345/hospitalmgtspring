package com.geemla.hospitalapp.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.geemla.hospitalapp.constants.RoleType;
import com.geemla.hospitalapp.domains.actors.User;
import com.geemla.hospitalapp.domains.base.NamedModel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Emoche
 **/

@Entity
@Table(name = "roles")
@Setter
@Getter
public class Role extends NamedModel {
    private static final long serialVersionUID = 1L;

    public Role(){

    }
    public Role(String name){
    	super.setName(name);
    }
    public Role(String name, String description){
    	super.setName(name);
    	this.setDescription(description);
    }
    public Role(String name, String description, Set<Permission> permissions, RoleType roleType){
        super.setName(name);
        super.setActive(true);
        this.setDescription(description);
        this.setPermissions(permissions);
        this.setRoleType(roleType);
    }

    @JsonIgnore
    @ManyToMany(mappedBy = "roles")
    private Set<User> users;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "role_permissions",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
    private Set<Permission> permissions;

    @Lob
    @Column(name="description")
    private String description;

    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    public Collection<String> getPermissionNames() {
        if (permissions == null) {
            permissions = new HashSet<>();
        }

        return permissions.stream().map(p -> p.getName()).collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return "Role{" + "id=" + this.getId() + "name=" + this.getName() + ", description=" + description + ", active=" + super.isActive() + '}';
    }
}
