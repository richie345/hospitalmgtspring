package com.geemla.hospitalapp.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomLogoutSuccessHandler implements LogoutSuccessHandler {


    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void onLogoutSuccess(HttpServletRequest request,
                                HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        // Code For Business Here
        if(null != authentication) {
            logger.info("Logout Sucessfully with Principal: " + authentication.getName());
        }

        response.setStatus(HttpServletResponse.SC_OK);
        //redirect to login
        response.sendRedirect("/account/login?info=You logged out successfully!");
    }

}
