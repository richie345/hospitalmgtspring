package com.geemla.hospitalapp.security;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Emoche
 **/

@Component
public class FailureLoginHandler implements AuthenticationFailureHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response,
                                        AuthenticationException e)
            throws IOException, ServletException {

        System.out.println("Failure login deteted");
        String errorMessage = ExceptionUtils.getMessage(e);

        String referer = request.getHeader("referer");

        // sendError(httpServletResponse, HttpServletResponse.SC_UNAUTHORIZED, errorMessage, e);
        System.out.println(errorMessage);
        System.out.println(e.toString());

        // if(e.getClass().isAssignableFrom(DisabledException.class)){
        // setDefaultFailureUrl("/accountRecovery");

        if (e.getClass().isAssignableFrom(BadCredentialsException.class)) {
            System.out.println("If block running...");
            errorMessage = "Invalid username and/or password";

            redirectStrategy.sendRedirect(request, response,"/account/login?error=" + errorMessage);
        }

        else if (e.getClass().isAssignableFrom(LockedException.class)) {
            System.out.println("Else if block running...");
            errorMessage = "Your account is locked out or suspended, contact an admin.html  <a href='/contact'> Contact Support</a>";


            response.sendRedirect("/account/login?error=" + errorMessage);
        }

        else{
            System.out.println("Else block running...");
            errorMessage = "Oops something went wrong..., contact an admin.html  <a href='/contact'> Contact Support</a>";

            response.sendRedirect("/account/login?error=" + errorMessage);

        }
    }
}
