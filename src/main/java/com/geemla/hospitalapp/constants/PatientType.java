package com.geemla.hospitalapp.constants;

/**
 * @author Emoche
 **/
public enum PatientType {
    INPATIENT,
    OUTPATIENT;

    private PatientType() {
    }
}
