package com.geemla.hospitalapp.constants;

public enum RoleType {
    DOCTOR,
    BACKOFFICE;

    private RoleType() {
    }
}
