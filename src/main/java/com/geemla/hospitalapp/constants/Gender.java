package com.geemla.hospitalapp.constants;

public enum Gender {
    M ("Male"),
    F ("Female"),
    Male ("Male"),
    Female ("Female");;

    private String label;

    public String getLabel()
    {
        return this.label;
    }

    private Gender(String label) {
        this.label = label;
    }
}
