package com.geemla.hospitalapp.constants;

public enum BackOfficeType {
    ADMIN,
    OTHERS;

    private BackOfficeType() { }
}
