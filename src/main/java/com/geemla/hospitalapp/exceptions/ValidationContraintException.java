package com.geemla.hospitalapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Bamtak
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Some constraints are violated ...")
public class ValidationContraintException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ValidationContraintException(String message) {
        super(message);
    }
}
