package com.geemla.hospitalapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class PaymentVerificationException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 7425166491627306473L;

    public PaymentVerificationException() {
        super();
    }

    public PaymentVerificationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public PaymentVerificationException(final String message) {
        super(message);
    }

    public PaymentVerificationException(final Throwable cause) {
        super(cause);
    }
}
