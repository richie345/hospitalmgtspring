package com.geemla.hospitalapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ObjectAlreadyExistException extends RuntimeException {
    public ObjectAlreadyExistException(String msg, Throwable t) {
        super(msg, t);
    }

    public ObjectAlreadyExistException(String msg) {
        super(msg);
    }
}
