package com.geemla.hospitalapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidAmountException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 7425166491627306473L;

    public InvalidAmountException() {
        super();
    }

    public InvalidAmountException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InvalidAmountException(final String message) {
        super(message);
    }

    public InvalidAmountException(final Throwable cause) {
        super(cause);
    }
}
