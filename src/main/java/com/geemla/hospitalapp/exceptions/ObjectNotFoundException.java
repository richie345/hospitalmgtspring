package com.geemla.hospitalapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ObjectNotFoundException extends RuntimeException {
    public ObjectNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }

    public ObjectNotFoundException(String msg) {
        super(msg);
    }
}
