package com.geemla.hospitalapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidMatchingPasswordException extends RuntimeException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidMatchingPasswordException(String msg, Throwable t) {
        super(msg, t);
    }

    public InvalidMatchingPasswordException(String msg) {
        super(msg);
    }
}
