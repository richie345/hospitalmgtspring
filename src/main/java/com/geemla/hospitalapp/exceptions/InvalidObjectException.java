package com.geemla.hospitalapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidObjectException extends RuntimeException {
    public InvalidObjectException(String msg, Throwable t) {
        super(msg, t);
    }

    public InvalidObjectException(String msg) {
        super(msg);
    }
}
