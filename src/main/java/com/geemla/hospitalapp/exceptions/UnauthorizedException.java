package com.geemla.hospitalapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason="You are not authorized to view this resource!")
public class UnauthorizedException extends RuntimeException {


    /**
     *
     */
    private static final long serialVersionUID = -8317180041955449452L;

    public UnauthorizedException() {
        super();
    }

    public UnauthorizedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UnauthorizedException(final String message) {
        super(message);
    }

    public UnauthorizedException(final Throwable cause) {
        super(cause);
    }
}
