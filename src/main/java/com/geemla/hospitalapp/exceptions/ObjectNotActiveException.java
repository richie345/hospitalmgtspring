package com.geemla.hospitalapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ObjectNotActiveException extends RuntimeException {
    public ObjectNotActiveException(String msg, Throwable t) {
        super(msg, t);
    }

    public ObjectNotActiveException(String msg) {
        super(msg);
    }
}
