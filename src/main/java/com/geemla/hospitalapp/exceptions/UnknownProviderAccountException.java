package com.geemla.hospitalapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class UnknownProviderAccountException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 7425166491627306473L;

    public UnknownProviderAccountException() {
        super();
    }

    public UnknownProviderAccountException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UnknownProviderAccountException(final String message) {
        super(message);
    }

    public UnknownProviderAccountException(final Throwable cause) {
        super(cause);
    }
}
