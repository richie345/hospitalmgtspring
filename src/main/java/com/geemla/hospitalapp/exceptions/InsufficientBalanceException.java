package com.geemla.hospitalapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InsufficientBalanceException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 7425166491627306473L;

    public InsufficientBalanceException() {
        super();
    }

    public InsufficientBalanceException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public InsufficientBalanceException(final String message) {
        super(message);
    }

    public InsufficientBalanceException(final Throwable cause) {
        super(cause);
    }
}
