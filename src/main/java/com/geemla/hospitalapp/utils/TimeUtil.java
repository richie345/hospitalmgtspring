package com.geemla.hospitalapp.utils;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class TimeUtil {

    public static String YYMMddHHmmss(){
        LocalDateTime localDateTime = LocalDateTime.now();
        String timeNow = localDateTime.format(DateTimeFormatter.ofPattern("YYMMddHHmmss"));
        return timeNow;
    }

    public static String getIsoTime(Timestamp timestamp) {
        if (timestamp == null) {
            return null;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        return timestamp.toLocalDateTime().format(formatter);
    }

    public static LocalDateTime now(){
        LocalDateTime localDateTime = LocalDateTime.now();
        return localDateTime;
    }

//    public static void testBackwardCompatability() {
//        Date currentDate = new Date();
//        System.out.println("Current date: " + currentDate);
//        //Get the instant of current date in terms of milliseconds
//        Instant now = currentDate.toInstant();
//        ZoneId currentZone = ZoneId.systemDefault();
//        LocalDateTime localDateTime = LocalDateTime.ofInstant(now, currentZone);
//        System.out.println("Local date: " + localDateTime);
//        ZonedDateTime zonedDateTime = ZonedDateTime.ofInstant(now, currentZone);
//        System.out.println("Zoned date: " + zonedDateTime);
//    }

    public static String localDateTime() {
        Date currentDate = new Date();
        Instant now = currentDate.toInstant();
        ZoneId currentZone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(now, currentZone);
        String time = localDateTime + "Z";
        System.out.println("Zoned date: " + time);
        return time;
    }

    public static String localTime(){
        LocalTime time = LocalTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        return time.format(formatter);
    }
}
