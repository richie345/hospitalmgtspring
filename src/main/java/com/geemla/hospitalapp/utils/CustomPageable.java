package com.geemla.hospitalapp.utils;

import lombok.Data;

@Data
public class CustomPageable {
    private int page = 0;
    private int size = 50;
    private String sortBy = "id";
    private String sortDirection = "desc";
}
