package com.geemla.hospitalapp.utils;

//import com.github.slugify.Slugify;
import com.geemla.hospitalapp.domains.actors.User;
import com.github.slugify.Slugify;
import com.google.common.base.Splitter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Olumayokun Fowowe on 28/09/2017.
 */

public class WebUtility {
    private static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
    private static final Pattern NONLATIN = Pattern.compile("[^\\w_-]");
    private static final Pattern SEPARATORS = Pattern.compile("[\\s\\p{Punct}&&[^-]]");
    private static final Pattern DATA_URL_PATTERN = Pattern.compile("^data:image/(.+?);base64,\\s*", Pattern.CASE_INSENSITIVE);

    public WebUtility() {
    }

    public static String hashPassword(String password) {
        return PASSWORD_ENCODER.encode(password);
    }

    public static boolean confirmPasswordIsCorrect(String password, String hashedPassword) {
        return PASSWORD_ENCODER.matches(password, hashedPassword);
    }

    public static boolean confirmMatchingPassword(String password, String matchingPassword) {
    	System.out.println("password: " + password + " and matchingPassword: " + matchingPassword);
        return password.equals(matchingPassword);
    }

    public static boolean isBcryptHashed(String password) {
        return StringUtils.isNotBlank(password) && password.length() == 60 && (password.startsWith("$2a$") || password.startsWith("$2b$") || password.startsWith("$2y$"));
    }

    public static boolean userExist(String username, String email) {
        User user = (User)Accessor.findOne(User.class, Filter.get().field("username", username).or().field("email", email));
        return user != null;
    }
    
    public static String sha256(String clearText) throws NoSuchAlgorithmException {
    	MessageDigest objSHA = MessageDigest.getInstance("SHA-256");
        byte[] bytSHA = objSHA.digest(clearText.getBytes(StandardCharsets.UTF_8));

        return bytesToHex(bytSHA).toUpperCase();
    }

    public static String sha512(String clearText) throws NoSuchAlgorithmException {
        MessageDigest objSHA = MessageDigest.getInstance("SHA-512");
        byte[] bytSHA = objSHA.digest(clearText.getBytes());
        BigInteger intNumber = new BigInteger(1, bytSHA);
        String strHashCode = intNumber.toString(16);

        while (strHashCode.length() < 128) {
            strHashCode = "0" + strHashCode;
        }
        return strHashCode;
    }
    
    public static String getClientIP(HttpServletRequest request) {
    	String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }
        
        if (remoteAddr.indexOf(":") > 0) {
        	remoteAddr = remoteAddr.substring(0, remoteAddr.indexOf(":")); 
        }

        return remoteAddr;
    }

    public static String generateRandomAlphaNumericString(int length){
        boolean useLetters = true;
        boolean useNumbers = true;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }

    public static String generateRandomNumericString(int length){
        boolean useLetters = false;
        boolean useNumbers = true;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }

    public static String generateNipSessionId(final String nipCode) {
        return nipCode + DateUtils.getDateAsString2(new Date()) + generateRandomBoundedLong(100000000000L, 999999999999L);
    }

    public static String generateSessionId() {
        return DateUtils.getDateAsString7(new Date()) + generateRandomBoundedLong(100000L, 999999L);
    }

    public static String generateNipSessionId(final String nipCode, final String reference) {
        return nipCode + DateUtils.getDateAsString2(new Date()) + reference;
    }

    public static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static Map<String, String> getQueryMap(String query)
    {
        if (query.indexOf("&text=") > 0) {
            query = query.substring(0, query.indexOf("&text="));
        }
//        query = query.replace(" & ", " And ");
//        query = query.replace(" &amp; ", " And ");
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params)
        {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }

    public static String encodeValue(String value) {
        try {
            if (!StringUtils.isEmpty(value)) {
                return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
            } else {
                return "";
            }
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
    }

    public static long generateRandomBoundedLong(long min, long max) {
        return min + (long) (Math.random() * (max - min));
    }

    public static int generateRandomIntegerBetweenRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static int generateRandomIntegerBetweenRange(int min, int max, Long seed) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        Random r = new Random(seed);
        return r.nextInt((max - min) + 1) + min;
    }

    public static Map<String, String> querySplitter(String url) {
        if (url.indexOf("?") > 0) {
            url = url.split("\\?")[1];
        }
        final Map<String, String> map = Splitter.on('&').trimResults().withKeyValueSeparator('=').split(url);
        return map;
    }

    public static String padLeft(String msg, int size) {
        return StringUtils.leftPad(msg, size, "0");
    }

    public static String padLeft(String msg, String padStr, int size) {
        return StringUtils.leftPad(msg, size, padStr);
    }

    public static String shuffle(String input){
        List<Character> characters = new ArrayList<Character>();
        for(char c:input.toCharArray()){
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while(characters.size()!=0){
            int randPicker = (int)(Math.random()*characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }

    public static int checksum(String cbnCode, String serial) {
        if (serial.length() != 9) {
            System.out.println("The serial must be 9 digits");
            return -1;
        } else {
            String newSerial = cbnCode + serial;
            int num = 0;
            int c = 0;
            int p = 0;
            for (int i = 0; i < 4; i++) {
                int j = Integer.parseInt(String.valueOf(newSerial.charAt(c)));
                p = j * 3;
                num = num + p;

                c++;
                j = Integer.parseInt(String.valueOf(newSerial.charAt(c)));
                p = j * 7;
                num = num + p;

                c++;
                j = Integer.parseInt(String.valueOf(newSerial.charAt(c)));
                p = j * 3;
                num = num + p;

                c++;
            }
            num = num % 10;
            System.out.println("Modulo: " + num);
            num = 10 - num;

            return num;
        }
    }

    public static String reverseString(String input) {
        StringBuilder input1 = new StringBuilder();
        input1.append(input);
        input1 = input1.reverse();

        return input1.toString();
    }

    public static String generateNuban(String identifier, String cbnCode) {
        String num = identifier + shuffle(padLeft(String.valueOf(generateRandomIntegerBetweenRange(1, 99999999)), 8));

        num = num + checksum(cbnCode, num);
        return num.substring(0, 10);
    }

    public static CloseableHttpClient getByPassedSSLHttpClient() {
        CloseableHttpClient httpClient
                = HttpClients.custom()
                .setSSLHostnameVerifier(new NoopHostnameVerifier())
                .build();

        return httpClient;
    }

    public static Pageable createPageRequest(CustomPageable customPageable) {
        Sort sort;
        if (customPageable.getSortDirection().equalsIgnoreCase("desc")){
            sort = Sort.by(Sort.Direction.DESC, customPageable.getSortBy());
        } else {
            sort = Sort.by(Sort.Direction.ASC, customPageable.getSortBy());
        }
        return PageRequest.of(customPageable.getPage(), customPageable.getSize(), sort);
    }

    public static boolean isBase64Image(String encodedImage) {
        if (encodedImage.startsWith("data:")) {
            final Matcher m = DATA_URL_PATTERN.matcher(encodedImage);
            if (m.find()) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static void writemBase64ImageToFile(String encodedImg, String fileName, boolean temp) throws IOException {
        byte[] decodedImg = Base64.getMimeDecoder().decode(encodedImg.split(",")[1]);
        String uploadRootPath = "upload";
        if (temp) {
            uploadRootPath = "tmp";
        }
        File uploadRootDir = new File(uploadRootPath);
        // Create directory if it not exists.
        if (!uploadRootDir.exists()) {
            uploadRootDir.mkdirs();
        }
        Path destinationFile = Paths.get(uploadRootDir.getAbsolutePath(), fileName);
        Files.write(destinationFile, decodedImg);
    }

    // Returns the image in a byte array
    public static byte[] readImage(String imageName, boolean temp) throws IOException {
        String uploadRootPath = "upload";
        if (temp) {
            uploadRootPath = "tmp";
        }
        File uploadRootDir = new File(uploadRootPath);
        File imageFile = new File(uploadRootDir.getAbsolutePath() + File.separator + imageName);
        byte[] fileContent = FileUtils.readFileToByteArray(imageFile);

        return fileContent;
    }

    public static Resource getImageFile(String imageName, boolean temp) throws IOException {
        String uploadRootPath = "upload";
        if (temp) {
            uploadRootPath = "tmp";
        }
        File uploadRootDir = new File(uploadRootPath);
        File imageFile = new File(uploadRootDir.getAbsolutePath() + File.separator + imageName);
        return new FileSystemResource(imageFile);
    }

    public static  <T> CompletableFuture<List<T>> allOf(List<CompletableFuture<T>> futuresList) {
        CompletableFuture<Void> allFuturesResult =
                CompletableFuture.allOf(futuresList.toArray(new CompletableFuture[futuresList.size()]));
        return allFuturesResult.thenApply(v ->
                futuresList.stream().
                        map(future -> future.join()).
                        collect(Collectors.<T>toList())
        );
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static String getOperatingSystem() {
        String os = SystemUtils.OS_NAME;
        return os;
    }

    public static String slugify(String req) {
        Slugify slug = new Slugify();
        return slug.slugify(req.toLowerCase());
    }
}
