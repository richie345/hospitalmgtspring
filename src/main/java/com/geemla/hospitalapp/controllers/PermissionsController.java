package com.geemla.hospitalapp.controllers;


import com.geemla.hospitalapp.security.Permission;
import com.geemla.hospitalapp.services.security.PermissionService;
import com.geemla.hospitalapp.utils.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping({"/settings/permissions"})
public class PermissionsController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    PermissionService permissionService;

    @RequestMapping(method = {RequestMethod.GET})
    @PreAuthorize("hasAnyRole('CAN_VIEW_ROLE', 'CAN_VIEW_ALL_TRANSACTIONS')")
    public String showPermissions(HttpServletRequest request, Model model) {
        List<Permission> permissions = this.permissionService.findAll();
        model.addAttribute("permissions", permissions);

        this.LOGGER.debug("Rendering permissions list page.");
        return "permissions/list_permissions2";
    }

    @RequestMapping(value = {"/add"}, method = {RequestMethod.POST})
    @PreAuthorize("hasAnyRole('CAN_ADD_ROLE')")
    @ResponseBody
    public GenericResponse addPermission(HttpServletRequest request, @Valid Permission permission) {
        this.LOGGER.info("Permission: {}", permission);
        this.permissionService.saveOrUpdate(permission);
        this.LOGGER.debug("Successfully created a permission");
        return new GenericResponse("success");
    }

    @RequestMapping(value = {"/edit"}, method = {RequestMethod.POST}, produces = {"application/json"})
    @PreAuthorize("hasAnyRole('CAN_ADD_ROLE')")
    @ResponseBody
    public GenericResponse updatePermission(HttpServletRequest request, @Valid Permission permission) {
        this.LOGGER.info("Permission: {}", permission);
        this.permissionService.saveOrUpdate(permission);
        this.LOGGER.debug("Successfully updated a permission");
        return new GenericResponse("success");
    }
}
