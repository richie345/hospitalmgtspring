package com.geemla.hospitalapp.controllers;

import com.geemla.hospitalapp.constants.BackOfficeType;
import com.geemla.hospitalapp.dto.actors.BackofficeUserDto;
import com.geemla.hospitalapp.security.Role;
import com.geemla.hospitalapp.services.interfaces.BackofficeUserService;
import com.geemla.hospitalapp.services.interfaces.UserService;
import com.geemla.hospitalapp.services.security.RoleService;
import com.geemla.hospitalapp.utils.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping({"/user-management/backoffice-users"})
public class BackofficeUsersController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    RoleService roleService;

    @Autowired
    BackofficeUserService backofficeUserService;

    @Autowired
    UserService userService;

    @RequestMapping(method = {RequestMethod.GET})
    @PreAuthorize("hasAnyRole('CAN_VIEW_USER')")
    public String showUsers(HttpServletRequest request, Model model) {
        List<Role> roles = this.roleService.findAllActiveBackofficeRoles();
        model.addAttribute("roles", roles);
        model.addAttribute("backOfficeTypes", BackOfficeType.values());
        model.addAttribute("backofficeUsers", this.backofficeUserService.findAll());
        this.LOGGER.debug("Rendering backoffice users list page.");
        return "backoffice_users/list_backoffice_users2";
    }

    @RequestMapping(value = {"/add"}, method = {RequestMethod.POST})
    @PreAuthorize("hasRole('CAN_ADD_USER')")
    @ResponseBody
    public GenericResponse addUser(HttpServletRequest request, @Valid BackofficeUserDto backofficeUserDto) {
        this.LOGGER.info("BackofficeUserDto: {}", backofficeUserDto);
        this.backofficeUserService.save(backofficeUserDto);
        this.LOGGER.debug("Successfully created a user");
        return new GenericResponse("success");
    }

    @RequestMapping(value = {"/edit"}, method = {RequestMethod.POST})
    @PreAuthorize("hasRole('CAN_ADD_USER')")
    @ResponseBody
    public GenericResponse updateUser(HttpServletRequest request, @Valid BackofficeUserDto backofficeUserDto) {
        this.LOGGER.info("BackofficeUserDto: {}", backofficeUserDto);
        this.backofficeUserService.update(backofficeUserDto);
        this.LOGGER.debug("Successfully updated a user");
        return new GenericResponse("success");
    }
}
