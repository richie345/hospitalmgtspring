package com.geemla.hospitalapp.controllers;

import com.geemla.hospitalapp.domains.Schedule;
import com.geemla.hospitalapp.domains.actors.Patient;
import com.geemla.hospitalapp.dto.ScheduleDto;
import com.geemla.hospitalapp.dto.actors.PatientDto;
import com.geemla.hospitalapp.exceptions.ObjectNotFoundException;
import com.geemla.hospitalapp.services.interfaces.PatientService;
import com.geemla.hospitalapp.services.interfaces.ScheduleService;
import com.geemla.hospitalapp.utils.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * @author Emoche
 **/
@Controller
@RequestMapping({"/settings/patients"})
public class PatientController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    PatientService patientService;

    @Autowired
    ScheduleService scheduleService;

    @RequestMapping(method = {RequestMethod.GET})
    @PreAuthorize("hasAnyRole('CAN_VIEW_PATIENT', 'CAN_VIEW_ALL_PATIENTS')")
    public String showFaculties(Model model) {
        List<Patient> patients = this.patientService.findAll();
        model.addAttribute("patients", patients);

        this.LOGGER.debug("Rendering patients list page.");
        return "default/dashboard2";

    }

    @RequestMapping(value = {"/add"}, method = {RequestMethod.POST})
    @PreAuthorize("hasAnyRole('CAN_ADD_PATIENT')")
    @ResponseBody
    public GenericResponse addPatient(HttpServletRequest request, @Valid PatientDto patientDto) {
        this.LOGGER.info("Patient: {}", patientDto);
        this.patientService.save(patientDto);
        this.LOGGER.debug("Successfully created a patient");
        return new GenericResponse("success");
    }

    @RequestMapping(value = {"/edit"}, method = {RequestMethod.POST}, produces = {"application/json"})
    @PreAuthorize("hasAnyRole('CAN_ADD_PATIENT')")
    @ResponseBody
    public GenericResponse updatePatient(@Valid PatientDto patientDto) {
        this.LOGGER.info("patient: {}", patientDto);
        this.patientService.update(patientDto);
        this.LOGGER.debug("Successfully updated a patient");
        return new GenericResponse("success");
    }

    @RequestMapping(value = {"/{patientUuid}/schedules"}, method = {RequestMethod.GET})
    @PreAuthorize("hasAnyRole('CAN_ADD_PATIENT')")
    public String showSchedules(@PathVariable("patientUuid") String patientUuid, Model model) {
        Patient patient = this.patientService.findByUuid(patientUuid);
        if (patient == null)
            throw new ObjectNotFoundException("The patient does not exist!");
        List<Schedule> schedules = this.scheduleService.findAllByPatientId(patientUuid);
        model.addAttribute("patient", patient);
        model.addAttribute("schedules", schedules);

        this.LOGGER.debug("Rendering schedules list page.");
        return "settings/schedules2";
    }

    @RequestMapping(value = {"/{patientUuid}/schedules/add"}, method = {RequestMethod.POST})
    @PreAuthorize("hasAnyRole('CAN_ADD_PATIENT')")
    @ResponseBody
    public GenericResponse addDepartment(@PathVariable("patientUuid") String patientUuid, @Valid ScheduleDto scheduleDto) {
        this.LOGGER.info("Schedule: {}", scheduleDto);
        this.scheduleService.save(scheduleDto);
        this.LOGGER.debug("Successfully created a schedule");
        return new GenericResponse("success");
    }

    @RequestMapping(value = {"/{patientUuid}/schedules/edit"}, method = {RequestMethod.POST}, produces = {"application/json"})
    @PreAuthorize("hasAnyRole('CAN_ADD_PATIENT')")
    @ResponseBody
    public GenericResponse updateDepartment(@PathVariable("patientUuid") String patientUuid, @Valid ScheduleDto scheduleDto) {
        this.LOGGER.info("scheduleDto: {}", scheduleDto);
        this.scheduleService.update(scheduleDto);
        this.LOGGER.debug("Successfully updated a schedule");
        return new GenericResponse("success");
    }
}
