package com.geemla.hospitalapp.controllers;

import com.geemla.hospitalapp.constants.RoleType;
import com.geemla.hospitalapp.domains.actors.User;
import com.geemla.hospitalapp.security.Role;
import com.geemla.hospitalapp.services.interfaces.PatientService;
import com.geemla.hospitalapp.services.interfaces.UserService;
import com.geemla.hospitalapp.services.security.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping({"/"})
public class DefaultController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    UserService userService;

    @Autowired
    RoleService roleService;


    @Autowired
    PatientService patientService;


    @RequestMapping(method = {RequestMethod.GET})
    public String showHome(HttpServletRequest request, Model model) {
        return "redirect:/dashboard";
    }

    @RequestMapping(value = {"/dashboard"}, method = {RequestMethod.GET})
    public String showDashboard(Model model) {
        this.LOGGER.debug("Rendering Dashboard page.");
        User user = this.userService.getLoginUserElseThrow();
        Role role = user.getFirstRole();
        if (role.getRoleType().name().equalsIgnoreCase(RoleType.BACKOFFICE.name())) {
            model.addAttribute("patients", this.patientService.findAll());
            return "default/dashboard2";
        }
        if (role.getRoleType().name().equalsIgnoreCase(RoleType.DOCTOR.name())) {
            //model.addAttribute("invoices", this.invoiceService.findTop10ByUserOrderByIdDesc(user));
            return "doctor_dashboard2";
        }
        //model.addAttribute("studentsCount", this.studentService.countAllActive());
        return "default/dashboard2";
    }

    @RequestMapping(value = {"doc"}, method = {RequestMethod.GET})
    public String showDocumentation() {
        this.LOGGER.debug("Rendering OpenAPI Documentation page.");
        return "redirect:/swagger-ui.html";
    }
}
