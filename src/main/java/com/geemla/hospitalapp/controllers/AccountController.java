package com.geemla.hospitalapp.controllers;


import com.geemla.hospitalapp.domains.VerificationToken;
import com.geemla.hospitalapp.domains.actors.User;
import com.geemla.hospitalapp.dto.TokenDto;
import com.geemla.hospitalapp.dto.UsernameOrEmailDto;
import com.geemla.hospitalapp.dto.actors.PatientDto;
import com.geemla.hospitalapp.exceptions.InvalidMatchingPasswordException;
import com.geemla.hospitalapp.exceptions.ObjectNotFoundException;
import com.geemla.hospitalapp.services.interfaces.PatientService;
import com.geemla.hospitalapp.services.interfaces.UserService;
import com.geemla.hospitalapp.utils.DateUtils;
import com.geemla.hospitalapp.utils.GenericResponse;
import com.geemla.hospitalapp.utils.WebUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Optional;

@Controller
@RequestMapping({"/account"})
public class AccountController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Autowired
    private Environment env;

    @Autowired
    PatientService patientService;

    @RequestMapping(value = {"profile"}, method = {RequestMethod.GET})
    @PreAuthorize("hasRole('CAN_CHANGE_PROFILE')")
    public String showProfile(HttpServletRequest request, Model model, Principal principal) {
        Optional<User> optionalUser = this.userService.getUserByUsernameOrEmail(principal.getName());
        User user = optionalUser.get();
        model.addAttribute("user", user);
        model.addAttribute("createdAt", DateUtils.getDateAsString5(user.getCreatedAt()));
        this.LOGGER.debug("Rendering profile page.");
        return "account/profile";
    }

    @RequestMapping(value = {"/register"}, method = {RequestMethod.GET})
    public String showRegisterForm(HttpServletRequest request, Model model) {
        this.LOGGER.debug("Rendering login page.");
        //model.addAttribute("institutions", this.institutionService.findAllActive());
        //model.addAttribute("studentTypes", StudentType.values());

        return "account/register2";
    }

    @RequestMapping(value = {"/register"}, method = {RequestMethod.POST})
    @ResponseBody
    public GenericResponse register(HttpServletRequest request, Model model, @Valid PatientDto requestDto) {
        this.LOGGER.info("user: {}", requestDto);
        this.patientService.save(requestDto);
        return new GenericResponse("success");
    }

    @RequestMapping(value = {"/login"}, method = {RequestMethod.GET})
    public String showLoginForm(HttpServletRequest request, Model model) {
        this.LOGGER.debug("Rendering login page.");

        return "account/login3";
    }


    @RequestMapping(value = {"/forgotpassword"}, method = {RequestMethod.GET})
    public String showForgotPasswordForm(HttpServletRequest request, Model model) {
        this.LOGGER.debug("Rendering forgot password page.");

        return "account/forgotpassword2";
    }

    @RequestMapping(value = {"/forgotpassword"}, method = {RequestMethod.POST})
    public String forgotPassword(HttpServletRequest request, Model model, @Valid UsernameOrEmailDto usernameOrEmailDto) {
        Optional<User> optional = this.userService.getUserByUsernameOrEmail(usernameOrEmailDto.getUsernameOrEmail());
        if (optional.isPresent()) {
            User user = optional.get();
            String token = WebUtility.generateRandomAlphaNumericString(6);
            this.userService.createPasswordResetTokenForUser(user, token);
            //this.emailService.sendForgotPasswordEmail(user, token);
            return "redirect:/account/login?info=Password reset email sent successfully.";
        }
        return "redirect:/account/login?error=The email is invalid";
    }

    private String getAppUrl(HttpServletRequest request) {
        return this.env.getProperty("webapp.baseUrl");
    }
}
