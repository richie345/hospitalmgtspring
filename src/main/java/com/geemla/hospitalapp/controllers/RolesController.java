package com.geemla.hospitalapp.controllers;

import com.geemla.hospitalapp.constants.RoleType;
import com.geemla.hospitalapp.dto.security.RoleDto;
import com.geemla.hospitalapp.dto.security.RolePermissionsDto;
import com.geemla.hospitalapp.security.Permission;
import com.geemla.hospitalapp.security.Role;
import com.geemla.hospitalapp.services.security.PermissionService;
import com.geemla.hospitalapp.services.security.RoleService;
import com.geemla.hospitalapp.utils.GenericResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping({"/settings/roles"})
public class RolesController {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    RoleService roleService;

    @Autowired
    PermissionService permissionService;

    @RequestMapping(method = {RequestMethod.GET})
    @PreAuthorize("hasAnyRole('CAN_VIEW_ROLE', 'CAN_VIEW_ALL_TRANSACTIONS')")
    public String showRoles(HttpServletRequest request, Model model) {
        List<Role> roles = this.roleService.findAll();
        model.addAttribute("roles", roles);
        model.addAttribute("roleTypes", RoleType.values());
        this.LOGGER.debug("Rendering roles list page.");
        return "roles/list_roles2";
    }

    @RequestMapping(value = {"/add"}, method = {RequestMethod.POST})
    @PreAuthorize("hasRole('CAN_ADD_ROLE')")
    @ResponseBody
    public GenericResponse addRole(HttpServletRequest request, @Valid RoleDto roleDto) {
        this.LOGGER.info("Role: {}", roleDto);
        this.roleService.save(roleDto);
        this.LOGGER.debug("Successfully created a role");
        return new GenericResponse("success");
    }

    @RequestMapping(value = {"/edit"}, method = {RequestMethod.POST})
    @PreAuthorize("hasRole('CAN_ADD_ROLE')")
    @ResponseBody
    public GenericResponse updateRole(HttpServletRequest request, @Valid RoleDto roleDto) {
        this.LOGGER.info("Role: {}", roleDto);
        this.roleService.update(roleDto);
        this.LOGGER.debug("Successfully updated a role");
        return new GenericResponse("success");
    }

    @RequestMapping(value = {"/permissions/{uuid}"}, method = {RequestMethod.GET})
    @PreAuthorize("hasRole('CAN_VIEW_ROLE')")
    public String showRolePermissionsPage(@PathVariable("uuid") String uuid, Model model) {
        this.LOGGER.debug("Rendering role permissions page.");
        Role role = this.roleService.findByUuid(uuid);
        List<Permission> permissions = this.permissionService.findAll();
        RolePermissionsDto rolePermissionsDto = new RolePermissionsDto();
        rolePermissionsDto.setId(role.getId());
        rolePermissionsDto.setUuid(role.getUuid());
        rolePermissionsDto.setName(role.getName());
        rolePermissionsDto.setSelectedPermissions(role.getPermissionNames());
        model.addAttribute("rolePermissionsDto", rolePermissionsDto);
        model.addAttribute("permissions", permissions);
        return "roles/permissions2";
    }

    @RequestMapping(value = {"/permissions/{uuid}"}, method = {RequestMethod.POST})
    @PreAuthorize("hasRole('CAN_ADD_ROLE')")
    @ResponseBody
    public GenericResponse updateRolePermissions(@PathVariable("uuid") String uuid, RolePermissionsDto rolePermissionsDto) {
        this.roleService.updatePermissions(rolePermissionsDto);
        this.LOGGER.debug("Role permissions updated successfully");
        return new GenericResponse("success");
    }
}
