package com.geemla.hospitalapp.services;

import com.geemla.hospitalapp.domains.actors.Patient;
import com.geemla.hospitalapp.dto.actors.PatientDto;
import com.geemla.hospitalapp.exceptions.ObjectAlreadyExistException;
import com.geemla.hospitalapp.exceptions.ObjectNotFoundException;
import com.geemla.hospitalapp.repositories.PatientRepository;
import com.geemla.hospitalapp.services.interfaces.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Emoche
 **/
@Service
@RequiredArgsConstructor
public class PatientServiceImpl extends BaseServiceImpl<Patient> implements PatientService {

    private final PatientRepository patientRepository;

    @Override
    public Patient save(PatientDto patientDto) {

        Patient patient = new Patient();
        patient.setFirstname(patientDto.getFirstname().toUpperCase());
        patient.setSurname(patientDto.getSurname().toUpperCase());
        patient.setOthername("");
        patient.setAddress(patientDto.getAddress().toUpperCase());
        patient.setBloodGroup("");
        patient.setDeathdate("");
        patient.setDob("");
        patient.setEmail(patientDto.getEmail().toUpperCase());
        patient.setFullNamesKin("");
        patient.setGender(patientDto.getGender());
        patient.setGenotype("");
        patient.setHospitalId("");
        patient.setKinAddress("");
        patient.setKinPhone("");
        patient.setMstatus("");
        patient.setNationality("");
        patient.setNospouse("");
        patient.setActive(true);
        patient.setEnabled(true);
        patient.setDeleted(false);

        return saveModel(patient);
    }

    @Override
    public Patient update(PatientDto patientDto) {
        Patient patient = getOne(patientDto.getId());
        if (patient == null) throw new ObjectNotFoundException("We're unable to locate the patient");

        patient.setActive(patientDto.isEnabled());

        try {
            return saveModel(patient);
        } catch (DataIntegrityViolationException e) {
            throw new ObjectAlreadyExistException("The patient name provided already exist!");
        }
    }

    @Override
    public Patient findByUuid(String uuid) {
        return patientRepository.findByUuid(uuid);
    }


    @Override
    public List<Patient> findAllActive() {
        return patientRepository.findAllByActive(true);
    }
}
