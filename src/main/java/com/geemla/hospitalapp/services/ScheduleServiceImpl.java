package com.geemla.hospitalapp.services;

import com.geemla.hospitalapp.domains.Schedule;
import com.geemla.hospitalapp.dto.ScheduleDto;
import com.geemla.hospitalapp.exceptions.ObjectAlreadyExistException;
import com.geemla.hospitalapp.exceptions.ObjectNotFoundException;
import com.geemla.hospitalapp.repositories.ScheduleRepository;
import com.geemla.hospitalapp.services.interfaces.ScheduleService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Emoche
 **/
@Service
@RequiredArgsConstructor
public class ScheduleServiceImpl extends BaseServiceImpl<Schedule> implements ScheduleService {

    private final ScheduleRepository scheduleRepository;

    @Override
    public Schedule save(ScheduleDto entryDetailsDto) {

        Schedule entryDetails = new Schedule();
        entryDetails.setPatientId(entryDetailsDto.getPatientId());
        entryDetails.setEntryId(entryDetailsDto.getEntryId());
        entryDetails.setAppointment_status_id(entryDetailsDto.getAppointment_status_id());
        entryDetails.setAppointment_status_note(entryDetailsDto.getAppointment_status_note());
        entryDetails.setDate_of_schedule(entryDetailsDto.getDate_of_schedule());
        entryDetails.setDoctor_assigned(entryDetailsDto.getDoctor_assigned());
        entryDetails.setEmail_remindal(entryDetailsDto.getEmail_remindal());
        entryDetails.setPatientType(entryDetailsDto.getPatientType());
        entryDetails.setPhone_remindal(entryDetailsDto.getPhone_remindal());
        entryDetails.setReason_of_schedule(entryDetailsDto.getReason_of_schedule());
        entryDetails.setRefferal_detail(entryDetailsDto.getRefferal_detail());
        entryDetails.setRegisteredBy(entryDetailsDto.getRegisteredBy());
        entryDetails.setSchedule_status(entryDetailsDto.getSchedule_status());
        entryDetails.setSchedule_type_id(entryDetailsDto.getSchedule_type_id());
        entryDetails.setTime_for_schedule(entryDetailsDto.getTime_for_schedule());


        return saveModel(entryDetails);
    }

    @Override
    public Schedule update(ScheduleDto patientDto) {
        Schedule schedule = getOne(patientDto.getId());
        if (schedule == null) throw new ObjectNotFoundException("We're unable to locate the patient");

        schedule.setSchedule_status(patientDto.getSchedule_status());

        try {
            return saveModel(schedule);
        } catch (DataIntegrityViolationException e) {
            throw new ObjectAlreadyExistException("The patient name provided already exist!");
        }
    }

    @Override
    public Schedule findByPatientId(String idd) {
        return scheduleRepository.findByPatientId(idd);
    }

    @Override
    public List<Schedule> findAllByPatientId(String idd) {
        return scheduleRepository.findAllByPatientId(idd);
    }

    @Override
    public Schedule findByEntryId(String idd) {
        return scheduleRepository.findByEntryId(idd);
    }


    @Override
    public List<Schedule> findAll() {
        return scheduleRepository.findAll();
    }
}
