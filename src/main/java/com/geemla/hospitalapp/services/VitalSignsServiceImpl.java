package com.geemla.hospitalapp.services;

import com.geemla.hospitalapp.domains.VitalSigns;
import com.geemla.hospitalapp.domains.actors.Patient;
import com.geemla.hospitalapp.dto.VitalSignsDto;
import com.geemla.hospitalapp.dto.actors.PatientDto;
import com.geemla.hospitalapp.exceptions.ObjectAlreadyExistException;
import com.geemla.hospitalapp.exceptions.ObjectNotFoundException;
import com.geemla.hospitalapp.repositories.PatientRepository;
import com.geemla.hospitalapp.repositories.VitalSignsRepository;
import com.geemla.hospitalapp.services.interfaces.VitalSignsService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Emoche
 **/
@Service
@RequiredArgsConstructor
public class VitalSignsServiceImpl extends BaseServiceImpl<VitalSigns> implements VitalSignsService {

    private final VitalSignsRepository vitalSignsRepository;

    @Override
    public VitalSigns save(VitalSignsDto vitalSignsDto) {

        VitalSigns vitalSigns = new VitalSigns();
        vitalSigns.setPatientAge(vitalSignsDto.getPatientAge().toUpperCase());
        vitalSigns.setActive(true);

        return saveModel(vitalSigns);
    }

    @Override
    public VitalSigns update(VitalSignsDto vitalSignsDto) {
        VitalSigns vitalSigns = getOne(vitalSignsDto.getId());
        if (vitalSigns == null) throw new ObjectNotFoundException("We're unable to locate the patient");

        vitalSigns.setActive(true);

        try {
            return saveModel(vitalSigns);
        } catch (DataIntegrityViolationException e) {
            throw new ObjectAlreadyExistException("The patient name provided already exist!");
        }
    }

    @Override
    public VitalSigns findByUuid(String uuid) {
        return vitalSignsRepository.findByUuid(uuid);
    }


    @Override
    public List<VitalSigns> findByPatientId(Patient patient) {
        return vitalSignsRepository.findByPatientId(patient);
    }
}
