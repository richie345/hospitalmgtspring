package com.geemla.hospitalapp.services;

import com.geemla.hospitalapp.domains.Diagnosis;
import com.geemla.hospitalapp.dto.DiagnosisDto;
import com.geemla.hospitalapp.repositories.DiagnosisRepository;
import com.geemla.hospitalapp.services.interfaces.DiagnosisService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Emoche
 **/
@Service
@RequiredArgsConstructor
public class DiagnosisServiceImpl extends BaseServiceImpl<Diagnosis> implements DiagnosisService {

    private final DiagnosisRepository diagnosisRepository;

    @Override
    public Diagnosis save(DiagnosisDto diagnosisDto) {

        Diagnosis diagnosis = new Diagnosis();
        diagnosis.setIDD(diagnosisDto.getIDD());
        diagnosis.setName(diagnosisDto.getName());
        diagnosis.setAccuracy(diagnosisDto.getAccuracy());
        diagnosis.setLcd(diagnosisDto.getLcd());
        diagnosis.setLcdName(diagnosisDto.getLcdName());
        diagnosis.setProfName(diagnosisDto.getProfName());
        diagnosis.setRanking(diagnosisDto.getRanking());

        return saveModel(diagnosis);
    }

    @Override
    public Diagnosis findByIDD(String idd) {
        return diagnosisRepository.findByIDD(idd);
    }


    @Override
    public List<Diagnosis> findAll() {
        return diagnosisRepository.findAll();
    }
}
