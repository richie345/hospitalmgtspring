package com.geemla.hospitalapp.services.security;

import com.geemla.hospitalapp.constants.RoleType;
import com.geemla.hospitalapp.dto.security.RoleDto;
import com.geemla.hospitalapp.dto.security.RolePermissionsDto;
import com.geemla.hospitalapp.exceptions.ObjectAlreadyExistException;
import com.geemla.hospitalapp.exceptions.ObjectNotFoundException;
import com.geemla.hospitalapp.repositories.security.RoleRepository;
import com.geemla.hospitalapp.security.Permission;
import com.geemla.hospitalapp.security.Role;
import com.geemla.hospitalapp.services.BaseServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleService extends BaseServiceImpl<Role> {

    private final RoleRepository roleRepository;
    private final PermissionService permissionService;

    public Role save(final RoleDto roleDto) {

        final Role role = new Role();

        role.setName(roleDto.getName().toUpperCase().trim());
        role.setDescription(roleDto.getDescription());
        role.setActive(true);
        role.setRoleType(roleDto.getRoleType());

        if (roleRepository.existsByName(role.getName().toUpperCase())){
            throw new ObjectAlreadyExistException("The role with name: " + roleDto.getName() + " already exist!");
        }

        return roleRepository.save(role);
    }

    public Role update(final RoleDto roleDto) {

        Role role = roleRepository.getOne(roleDto.getId());
        if (role == null){
            throw new ObjectNotFoundException("The role was not found");
        }
        role.setActive(roleDto.isActive());
        role.setName(roleDto.getName().toUpperCase().trim());
        role.setDescription(roleDto.getDescription());
        role.setRoleType(roleDto.getRoleType());

        try {
            return roleRepository.save(role);
        } catch (DataIntegrityViolationException e) {
            throw new ObjectAlreadyExistException("The role name provided already exist!");
        }
    }

    public void updatePermissions(RolePermissionsDto rolePermissionsDto) {
        Role role = roleRepository.findByUuid(rolePermissionsDto.getUuid());
        if (role == null){
            throw new ObjectNotFoundException("The role with UUID: " + rolePermissionsDto.getUuid() + " was not found");
        }

        List<Permission> permissions = new ArrayList<Permission>();
        if(rolePermissionsDto.getSelectedPermissions().size() > 0) {
            for(String permissionName: rolePermissionsDto.getSelectedPermissions()) {
                Permission permission = permissionService.findByName(permissionName);
                if (permission == null){
                    throw new ObjectNotFoundException("The permission with name: " + permissionName + " was not found");
                }
                permissions.add(permission);
            }
        }
        role.setPermissions(new HashSet<>(permissions));
        this.roleRepository.save(role);
    }

    public List<Role> findAllActiveBackofficeRoles() {
        return roleRepository.findAllByRoleTypeAndActive(RoleType.BACKOFFICE, true);
    }

    public Role findByUuid(String uuid) {
        return roleRepository.findByUuid(uuid);
    }

    public Role findByName(String name) {
        final Role role = roleRepository.findByName(name);;
        if (role == null) throw new ObjectNotFoundException("The role cannot be found!");
        return roleRepository.findByName(name);
    }

}
