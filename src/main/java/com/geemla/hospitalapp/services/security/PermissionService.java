package com.geemla.hospitalapp.services.security;

import com.geemla.hospitalapp.exceptions.ObjectAlreadyExistException;
import com.geemla.hospitalapp.exceptions.ValidationContraintException;
import com.geemla.hospitalapp.repositories.security.PermissionRepository;
import com.geemla.hospitalapp.security.Permission;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;


@Service
@RequiredArgsConstructor
public class PermissionService {

    private final PermissionRepository permissionRepository;

    public Permission saveOrUpdate(Permission permission){
        if(!validatePermission(permission.getName())) {
            throw new ValidationContraintException("Invalid Permission Name. You must append 'ROLE_' to the name. The name must not include a 'space'.");
        }

        try {
            permission.setActive(true);
            return permissionRepository.save(permission);
        } catch (DataIntegrityViolationException e) {
            throw new ObjectAlreadyExistException("The permission name provided already exist!");
        }
    }

    public List<Permission> findAll(){
        return permissionRepository.findAll();
    }


    public  Permission findById(Long id){
        return  permissionRepository.getOne(id);
    }

    public  Permission findByName(String name){
        return  permissionRepository.findByName(name);
    }

    public void saveAll(Collection<Permission> permissions) {
        permissionRepository.saveAll(permissions);
    }

    private boolean validatePermission(final String name) {
        if (name.toUpperCase().startsWith("ROLE_") && !name.contains(" ")) {
            return true;
        }
        return false;
    }

}
