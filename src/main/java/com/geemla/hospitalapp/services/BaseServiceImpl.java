package com.geemla.hospitalapp.services;

import com.geemla.hospitalapp.domains.base.Model;
import com.geemla.hospitalapp.exceptions.ObjectNotFoundException;
import com.geemla.hospitalapp.services.interfaces.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class BaseServiceImpl<T extends Model> implements BaseService<T> {

    private JpaRepository<T, Long> repository;

    public JpaRepository<T, Long> getRepository() {
        return repository;
    }

    @Autowired
    public void setRepository(JpaRepository<T, Long> repository) {
        this.repository = repository;
    }

    @Override
    public Optional<T> findOneById(Long Long) {
        return Objects.isNull(Long) ? Optional.empty() : repository.findById(Long);
    }

    @Override
    public T getOne(Long Long) {
        if (!this.existsById(Long)) throw  new ObjectNotFoundException("The Entity can not be found");
        return repository.getOne(Long);
    }

    @Override
    public boolean existsById(Long Long){
        return repository.existsById(Long);
    }

    @Override
    public boolean deleteModelById(Long Long) {
        return Objects.nonNull(Long) && this.existsById(Long) && delete(repository.getOne(Long));
    }

    @Override
    public boolean deleteModel(T model) {
        return Objects.nonNull(model) && this.existsById(model.getId()) && this.delete(model);
    }

    @Override
    public Long count() {
        return repository.count();
    }

    private boolean delete(T model) {
        model.delete();
        repository.save(model);
        return true;
    }

    @Override
    public List<T> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<T> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    public Optional<T> update(@NotNull(message = " Cannot update a null entity") T model) {
        return Objects.isNull(model) || objectDoesNotExist(model)
                ? Optional.empty() : Optional.of(this.updateModel(model));
    }

    private boolean objectDoesNotExist(T model) {
        return Objects.isNull(model.getId()) || !repository.existsById(model.getId());
    }

    private T updateModel(T model) {
        T object = repository.getOne(model.getId());
        model.setCreatedAt(object.getCreatedAt());
        return repository.save(model);
    }

    @Override
    public Optional<T> save(@NotNull(message = "Cannot saveOrUpdate a null entity") T model) {
        return Objects.isNull(model) ? Optional.empty() : Optional.of(repository.save(model));
    }

    @Override
    public T saveModel(@NotNull(message = "Cannot saveOrUpdate a null entity") T model) {
        return repository.save(model);
    }

    @Override
    public List<T> saveAll(List<T> models) {
        return repository.saveAll(models);
    }


    @Override
    public void deleteAll(List<T> models) {
        for (T model : models) deleteModelById(model.getId());
    }

}
