package com.geemla.hospitalapp.services;

import com.geemla.hospitalapp.domains.Symptoms;
import com.geemla.hospitalapp.dto.SymptomsDto;
import com.geemla.hospitalapp.exceptions.ObjectAlreadyExistException;
import com.geemla.hospitalapp.exceptions.ObjectNotFoundException;
import com.geemla.hospitalapp.repositories.SymptomsRepository;
import com.geemla.hospitalapp.services.interfaces.SymptomsService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Emoche
 **/
@Service
@RequiredArgsConstructor
public class SymptomsServiceImpl extends BaseServiceImpl<Symptoms> implements SymptomsService {

    private final SymptomsRepository symptomsRepository;

    @Override
    public Symptoms save(SymptomsDto symptomsDto) {

        Symptoms symptoms = new Symptoms();
        symptoms.setIDD(symptomsDto.getIDD());
        symptoms.setName(symptomsDto.getName());

        return saveModel(symptoms);
    }

    @Override
    public Symptoms findByIDD(String idd) {
        return symptomsRepository.findByIDD(idd);
    }


    @Override
    public List<Symptoms> findAll() {
        return symptomsRepository.findAll();
    }
}
