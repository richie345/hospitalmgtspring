package com.geemla.hospitalapp.services.actors;

import com.geemla.hospitalapp.domains.PasswordResetToken;
import com.geemla.hospitalapp.domains.VerificationToken;
import com.geemla.hospitalapp.domains.actors.User;
import com.geemla.hospitalapp.dto.actors.UserDto;
import com.geemla.hospitalapp.exceptions.*;
import com.geemla.hospitalapp.repositories.PasswordResetTokenRepository;
import com.geemla.hospitalapp.repositories.VerificationTokenRepository;
import com.geemla.hospitalapp.repositories.actors.UserRepository;
import com.geemla.hospitalapp.repositories.security.RoleRepository;
import com.geemla.hospitalapp.security.CustomUserFactory;
import com.geemla.hospitalapp.security.Permission;
import com.geemla.hospitalapp.security.Role;
import com.geemla.hospitalapp.security.SecurityContextFacade;
import com.geemla.hospitalapp.services.interfaces.UserService;
import com.geemla.hospitalapp.utils.Accessor;
import com.geemla.hospitalapp.utils.Filter;
import com.geemla.hospitalapp.utils.WebUtility;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.logging.Logger;

/**
 * @author Emoche
 **/

@Service
@Transactional
public class UserServiceImpl implements UserService, UserDetailsService {
    private final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserRepository repository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private VerificationTokenRepository tokenRepository;

    @Autowired
    private PasswordResetTokenRepository passwordTokenRepository;

    /*@Autowired
    EmailService emailService;*/

//    @Autowired
//    private PasswordEncoder passwordEncoder;

    public static final String TOKEN_INVALID = "invalidToken";
    public static final String TOKEN_EXPIRED = "expired";
    public static final String TOKEN_VALID = "valid";
    private final SecurityContextFacade securityContextFacade;

    public UserServiceImpl(UserRepository repository,
                           PasswordResetTokenRepository passwordTokenRepository,
                           SecurityContextFacade securityContextFacade,
                           VerificationTokenRepository tokenRepository,
                           RoleRepository roleRepository) {
        this.passwordTokenRepository = passwordTokenRepository;
        this.securityContextFacade = securityContextFacade;
        this.tokenRepository = tokenRepository;
        this.roleRepository = roleRepository;
        this.repository = repository;
    }

    @Override
    public User createUser(final UserDto accountDto) {
        if (userExist(accountDto.getEmail(), accountDto.getEmail())) {
            throw new UserAlreadyExistException("There is an existing account with email: " + accountDto.getEmail());
        }
        User user = new User();

        List<Role> roles = new ArrayList<Role>();
        Role role = null;
        for (String roleName: accountDto.getRoleNames()) {
        	role = roleRepository.findByName(roleName);
        	if (role == null){
                throw new ObjectNotFoundException("The role with name: " + roleName + " was not found");
            }
        	roles.add(roleRepository.findByName(roleName));
        }
        user.setFirstName(accountDto.getFirstName());
        user.setLastName(accountDto.getLastName());
        user.setPassword(WebUtility.generateRandomAlphaNumericString(8));
        user.setEmail(accountDto.getEmail());
        user.setRoles(new HashSet<>(roles));
        user = repository.save(user);

        //sendRegistrationEmail(user);
        return user;
    }
    
    @Override
    public void updateUser(final UserDto accountDto) {
    	final User user = this.findUserByID(accountDto.getId());
    	if (user == null){
            throw new UserNotFoundException("The user cannot be found!");
        }
    	
    	List<Role> roles = new ArrayList<Role>();
        Role role = null;
        for (String roleName: accountDto.getRoleNames()) {
        	role = roleRepository.findByName(roleName);
        	if (role == null){
                throw new ObjectNotFoundException("The role with name: " + roleName + " was not found");
            }
        	roles.add(roleRepository.findByName(roleName));
        }
    	
    	user.setFirstName(accountDto.getFirstName());
    	user.setLastName(accountDto.getLastName());
    	user.setEnabled(accountDto.isEnabled());
    	user.setRoles(new HashSet<>(roles));
    	
        repository.save(user);
    }
    
    @Override
    public void updateLastLogIn(final User user) {
    	user.setLastLoggedIn(new Date());
    	repository.save(user);
    }

    public User getUser(final String verificationToken) {
        final VerificationToken token = tokenRepository.findByToken(verificationToken);
        if (token != null) {
            return token.getUser();
        }
        return null;
    }

    @Override
    public VerificationToken getVerificationToken(final String VerificationToken) {
        return tokenRepository.findByToken(VerificationToken);
    }

    @Override
    public void deleteUser(final User user) {
        final VerificationToken verificationToken = tokenRepository.findByUser(user);

        if (verificationToken != null) {
            tokenRepository.delete(verificationToken);
        }

        final PasswordResetToken passwordToken = passwordTokenRepository.findByUser(user);

        if (passwordToken != null) {
            passwordTokenRepository.delete(passwordToken);
        }

        repository.delete(user);
    }

    @Override
    public void createVerificationTokenForUser(final User user, final String token) {
        final VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    @Override
    public Optional<VerificationToken> generateNewVerificationToken(final String existingVerificationToken) {
        VerificationToken vToken = tokenRepository.findByToken(existingVerificationToken);
        vToken.updateToken(UUID.randomUUID().toString());
        vToken = tokenRepository.save(vToken);
        return Optional.ofNullable(vToken);
    }

    @Override
    public void createPasswordResetTokenForUser(final User user, final String token) {
        final PasswordResetToken myToken = new PasswordResetToken(token, user);
        passwordTokenRepository.save(myToken);
    }

    @Override
    public User findByUsername (final String username){
        return  repository.findByUsername(username);
    }

    @Override
    public User findUserByEmail(final String email) {
        return repository.findByEmail(email);
    }

    @Override
    public User findUserByUiid(String uiid) {
        return repository.findByUuid(uiid);
    }

    @Override
    public User findUserByID(final long id) {
        return repository.getOne(id);
    }
    
    @Override
    public User findByUsernameOrEmail(String usernameOrEmail) {
    	return Accessor.findOne(User.class, Filter.get().field("username", usernameOrEmail).or().field("email", usernameOrEmail));
    }

    @Override
    public Collection<User> findAllUsers(){
        return repository.findAll();
    }

    @Override
    public PasswordResetToken getPasswordResetToken(final String token) {
        return passwordTokenRepository.findByToken(token);
    }

    @Override
    public User getUserByPasswordResetToken(final String token) {
        return passwordTokenRepository.findByToken(token).getUser();
    }

    /*@Override
    public void changeUserPassword(final User user, final ChangePasswordDto changePasswordDto) {
    	if (!this.checkIfValidOldPassword(user, changePasswordDto.getOldPassword())) {
    		throw new InvalidOldPasswordException("The old password entered is invalid!");
    	}
    	
    	if (!WebUtility.confirmMatchingPassword(changePasswordDto.getPassword(), changePasswordDto.getConfirmPassword())) {
    		throw new InvalidMatchingPasswordException("The password and confirmPassword fields do not match!");
    	}
        user.setPassword(changePasswordDto.getPassword());
        repository.save(user);
    }*/

    @Override
    public boolean checkIfValidOldPassword(final User user, final String oldPassword) {
        return WebUtility.confirmPasswordIsCorrect(oldPassword, user.getPassword());
    }

    /*@Override
    public String validateVerificationToken(RegistrationConfirmationDto requestDto) {
    	if (!WebUtility.confirmMatchingPassword(requestDto.getPassword(), requestDto.getConfirmPassword())) {
    		throw new InvalidMatchingPasswordException("The password and confirmPassword fields do not match!");
    	}
    	
        final VerificationToken verificationToken = tokenRepository.findByToken(requestDto.getToken());
        if (verificationToken == null) {
            return TOKEN_INVALID;
        }

        final User user = verificationToken.getUser();
        final Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            tokenRepository.delete(verificationToken);
            return TOKEN_EXPIRED;
        }

        user.setEnabled(true);
        user.setPassword(WebUtility.hashPassword(requestDto.getPassword()));
        repository.save(user);
        return TOKEN_VALID;
    }*/

    @Override
    public Optional<User> getUserByUsernameOrEmail(final String usernameOrEmail) {
        System.out.println("Checking if user with username or email: " + usernameOrEmail + " exist");
        return Optional.ofNullable(Accessor.findOne(User.class, Filter.get().field("username", usernameOrEmail).or().field("email", usernameOrEmail)));
    }
    
    @Override
    public String validatePasswordResetToken(String token, String userUuid) {
        final PasswordResetToken passToken = passwordTokenRepository.findByToken(token);
        if ((passToken == null) || !(passToken.getUser().getUuid()
                .equalsIgnoreCase(userUuid))) {
            return "invalidToken";
        }

        final Calendar cal = Calendar.getInstance();
        if ((passToken.getExpiryDate()
                .getTime() - cal.getTime()
                .getTime()) <= 0) {
            return "expired";
        }
        return null;
    }
    
    /*@Override
    public void saveResetPassword(final ResetPasswordDto resetPasswordDto) {
        final PasswordResetToken passToken = passwordTokenRepository.findByToken(resetPasswordDto.getToken());
        if ((passToken == null) || !(passToken.getUser().getUuid()
                .equalsIgnoreCase(resetPasswordDto.getUserUuid()))) {
            throw new ObjectNotFoundException("User not found. Token and UUID could not be validated");
        }
        final User user = passToken.getUser();

        if (!WebUtility.confirmMatchingPassword(resetPasswordDto.getPassword(), resetPasswordDto.getMatchingPassword())) {
            throw  new InvalidMatchingPasswordException("The password and the matchingPassword fields do not match");
        }
        user.setPassword(WebUtility.hashPassword(resetPasswordDto.getPassword()));
        repository.save(user);
    }*/

    @Override
    public Optional<User> getLoggedInUser() {
        String username = securityContextFacade.getPrincipal();
        LOGGER.info("logged in user: {}", username);
        return username == null || Objects.equals("anonymousUser", username)
                ? Optional.empty() : Optional.of(this.findUserByEmail(username));
    }

    @Override
    public User getLoginUserElseThrow() {
        Logger.getGlobal().info("Request for login user");
        return this.getLoggedInUser().orElseThrow(() -> new ObjectNotFoundException("You must Login for this Operation"));
    }
    
    private boolean userExist(final String username, final String email) {
        return Accessor.findOne(User.class, Filter.get().field("username", username).or().field("email", email)) != null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.findByUsernameOrEmail(username);

        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username/email '%s'.", username));
        } else {
            return CustomUserFactory.create(user, getAuthorities(user.getRoles()));
        }
    }

    /*@Override
    public void sendRegistrationEmail(final User user) {
        final String token = WebUtility.generateRandomAlphaNumericString(6);
        this.createVerificationTokenForUser(user, token);
        emailService.sendRegistrationEmail(user, token);
    }*/

    private final Collection<? extends GrantedAuthority> getAuthorities(final Collection<Role> roles) {
        return getGrantedAuthorities(getPrivileges(roles));
    }

    private final List<String> getPrivileges(final Collection<Role> roles) {
        final List<String> permissions = new ArrayList<String>();
        final List<Permission> collection = new ArrayList<Permission>();
        for (final Role role : roles) {
            collection.addAll(role.getPermissions());
        }
        for (final Permission item : collection) {
            permissions.add(item.getName());
        }

        return permissions;
    }

    private final List<GrantedAuthority> getGrantedAuthorities(final List<String> privileges) {
        final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (final String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
}
