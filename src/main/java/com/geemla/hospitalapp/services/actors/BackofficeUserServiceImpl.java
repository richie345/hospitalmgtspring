package com.geemla.hospitalapp.services.actors;

import com.geemla.hospitalapp.domains.actors.BackofficeUser;
import com.geemla.hospitalapp.domains.actors.User;
import com.geemla.hospitalapp.dto.actors.BackofficeUserDto;
import com.geemla.hospitalapp.exceptions.CustomException;
import com.geemla.hospitalapp.exceptions.ObjectNotFoundException;
import com.geemla.hospitalapp.repositories.actors.BackofficeUserRepository;
import com.geemla.hospitalapp.services.BaseServiceImpl;
import com.geemla.hospitalapp.services.interfaces.BackofficeUserService;
import com.geemla.hospitalapp.services.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BackofficeUserServiceImpl extends BaseServiceImpl<BackofficeUser> implements BackofficeUserService {

    private final BackofficeUserRepository backofficeUserRepository;
    private final UserService userService;

    @Override
    public BackofficeUser save(BackofficeUserDto backofficeUserDto) {
        final User user = userService.createUser(backofficeUserDto);
        if (user != null) {
            final BackofficeUser backofficeUser = new BackofficeUser();
            backofficeUser.setUser(user);
            backofficeUser.setBackOfficeType(backofficeUserDto.getBackOfficeType());
            backofficeUser.setActive(true);

            return saveModel(backofficeUser);
        }
        throw new CustomException("BackofficeUser creation failed!");
    }

    @Override
    public BackofficeUser update(BackofficeUserDto backofficeUserDto) {
        BackofficeUser backofficeUser = getOne(backofficeUserDto.getId());
        if (backofficeUser == null) throw new ObjectNotFoundException("We're unable to locate the backofficeUser");

        backofficeUserDto.setEnabled(backofficeUserDto.isActive());
        userService.updateUser(backofficeUserDto);

        backofficeUser.setActive(backofficeUserDto.isActive());
        backofficeUser.setBackOfficeType(backofficeUserDto.getBackOfficeType());

        return saveModel(backofficeUser);
    }

    @Override
    public BackofficeUser findByUuid(String uuid) {
        return backofficeUserRepository.findByUuid(uuid);
    }

    @Override
    public Long countAllActive() {
        return backofficeUserRepository.countAllByActive(true);
    }
}
