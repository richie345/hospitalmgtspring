package com.geemla.hospitalapp.services;

import com.geemla.hospitalapp.domains.Specializations;
import com.geemla.hospitalapp.dto.SpecializationDto;
import com.geemla.hospitalapp.repositories.SpecializationsRepository;
import com.geemla.hospitalapp.services.interfaces.SpecializationsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Emoche
 **/
@Service
@RequiredArgsConstructor
public class SpecializationsServiceImpl extends BaseServiceImpl<Specializations> implements SpecializationsService {

    private final SpecializationsRepository specializationsRepository;

    @Override
    public Specializations save(SpecializationDto specializationDto) {

        Specializations specializations = new Specializations();
        specializations.setIDD(specializationDto.getIDD());
        specializations.setName(specializationDto.getName());
        specializations.setIssueID(specializationDto.getIssueID());
        specializations.setSpecialistID(specializationDto.getSpecialistID());

        return saveModel(specializations);
    }

    @Override
    public Specializations findByIDD(String idd) {
        return specializationsRepository.findByIDD(idd);
    }


    @Override
    public List<Specializations> findAll() {
        return specializationsRepository.findAll();
    }
}
