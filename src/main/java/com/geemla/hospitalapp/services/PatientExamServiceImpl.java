package com.geemla.hospitalapp.services;

import com.geemla.hospitalapp.domains.PatientExam;
import com.geemla.hospitalapp.dto.PatientExamDto;
import com.geemla.hospitalapp.exceptions.ObjectAlreadyExistException;
import com.geemla.hospitalapp.exceptions.ObjectNotFoundException;
import com.geemla.hospitalapp.repositories.PatientExamRepository;
import com.geemla.hospitalapp.repositories.PatientRepository;
import com.geemla.hospitalapp.services.interfaces.PatientExamService;
import com.geemla.hospitalapp.services.interfaces.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Emoche
 **/
@Service
@RequiredArgsConstructor
public class PatientExamServiceImpl extends BaseServiceImpl<PatientExam> implements PatientExamService {

    private final PatientExamRepository patientRepository;

    @Override
    public PatientExam save(PatientExamDto entryDetailsDto) {

        PatientExam entryDetails = new PatientExam();
        entryDetails.setPatientId(entryDetailsDto.getPatientId());
        entryDetails.setEntryId(entryDetailsDto.getEntryId());
        entryDetails.setAllergies(entryDetailsDto.getAllergies());
        entryDetails.setAssessment(entryDetailsDto.getAssessment());
        entryDetails.setCarriedOutBy(entryDetailsDto.getCarriedOutBy());
        entryDetails.setDateCarriedOut(entryDetailsDto.getDateCarriedOut());
        entryDetails.setDrugHistory(entryDetailsDto.getDrugHistory());
        entryDetails.setFamilyHistory(entryDetailsDto.getFamilyHistory());
        entryDetails.setHistoryOfPresentIllness(entryDetailsDto.getHistoryOfPresentIllness());
        entryDetails.setLab(entryDetailsDto.getLab());
        entryDetails.setMedications(entryDetailsDto.getMedications());
        entryDetails.setOccupationalHistory(entryDetailsDto.getOccupationalHistory());
        entryDetails.setPastMedicalHistory(entryDetailsDto.getPastMedicalHistory());
        entryDetails.setPhysicalMentalExamination(entryDetailsDto.getPhysicalMentalExamination());
        entryDetails.setPresentingComplain(entryDetailsDto.getPresentingComplain());
        entryDetails.setRemarks(entryDetailsDto.getRemarks());
        entryDetails.setReviewOfSystems(entryDetailsDto.getReviewOfSystems());
        entryDetails.setSocialHistory(entryDetailsDto.getSocialHistory());
        entryDetails.setTitle(entryDetailsDto.getTitle());
        entryDetails.setValid(entryDetailsDto.getValid());


        return saveModel(entryDetails);
    }

    @Override
    public PatientExam update(PatientExamDto patientDto) {
        PatientExam patient = getOne(patientDto.getId());
        if (patient == null) throw new ObjectNotFoundException("We're unable to locate the patient");

        patient.setValid(patientDto.getValid());

        try {
            return saveModel(patient);
        } catch (DataIntegrityViolationException e) {
            throw new ObjectAlreadyExistException("The patient name provided already exist!");
        }
    }

    @Override
    public PatientExam findByPatientId(String idd) {
        return patientRepository.findByPatientId(idd);
    }

    @Override
    public PatientExam findByEntryId(String idd) {
        return patientRepository.findByEntryId(idd);
    }


    @Override
    public List<PatientExam> findAll() {
        return patientRepository.findAll();
    }
}
