package com.geemla.hospitalapp.services;

import com.geemla.hospitalapp.domains.EntryDetails;
import com.geemla.hospitalapp.dto.EntryDetailsDto;
import com.geemla.hospitalapp.repositories.EntryDetailsRepository;
import com.geemla.hospitalapp.services.interfaces.EntryDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Emoche
 **/
@Service
@RequiredArgsConstructor
public class EntryDetailsServiceImpl extends BaseServiceImpl<EntryDetails> implements EntryDetailsService {

    private final EntryDetailsRepository entryDetailsRepository;

    @Override
    public EntryDetails save(EntryDetailsDto entryDetailsDto) {

        EntryDetails entryDetails = new EntryDetails();
        entryDetails.setPatientId(entryDetailsDto.getPatientId());
        entryDetails.setEntryId(entryDetailsDto.getEntryId());
        entryDetails.setEntryType(entryDetailsDto.getEntryType());
        entryDetails.setRegisteredBy(entryDetailsDto.getRegisteredBy());

        return saveModel(entryDetails);
    }

    @Override
    public EntryDetails findByPatientId(String idd) {
        return entryDetailsRepository.findByPatientId(idd);
    }

    @Override
    public EntryDetails findByEntryId(String idd) {
        return entryDetailsRepository.findByEntryId(idd);
    }


    @Override
    public List<EntryDetails> findAll() {
        return entryDetailsRepository.findAll();
    }
}
