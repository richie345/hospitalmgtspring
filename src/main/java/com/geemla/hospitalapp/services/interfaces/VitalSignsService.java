package com.geemla.hospitalapp.services.interfaces;

import com.geemla.hospitalapp.domains.VitalSigns;
import com.geemla.hospitalapp.domains.actors.Patient;
import com.geemla.hospitalapp.dto.VitalSignsDto;

import java.util.List;

/**
 * @author Emoche
 **/
public interface VitalSignsService extends BaseService<VitalSigns> {
    VitalSigns save(VitalSignsDto vitalSignsDto);
    VitalSigns update(VitalSignsDto vitalSignsDto);
    VitalSigns findByUuid(String uuid);
    List<VitalSigns> findByPatientId(Patient patient);
}
