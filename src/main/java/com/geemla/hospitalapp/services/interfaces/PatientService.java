package com.geemla.hospitalapp.services.interfaces;

import com.geemla.hospitalapp.domains.actors.Patient;
import com.geemla.hospitalapp.dto.actors.PatientDto;

import java.util.List;

/**
 * @author Emoche
 **/
public interface PatientService extends BaseService<Patient> {
    Patient save(PatientDto patientDto);
    Patient update(PatientDto patientDto);
    Patient findByUuid(String uuid);
    List<Patient> findAllActive();
}
