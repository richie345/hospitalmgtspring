package com.geemla.hospitalapp.services.interfaces;

import com.geemla.hospitalapp.domains.Diagnosis;
import com.geemla.hospitalapp.dto.DiagnosisDto;

import java.util.List;

/**
 * @author Emoche
 **/
public interface DiagnosisService extends BaseService<Diagnosis> {
    Diagnosis save(DiagnosisDto diagnosisDto);
    Diagnosis findByIDD(String idd);
    List<Diagnosis> findAll();
}
