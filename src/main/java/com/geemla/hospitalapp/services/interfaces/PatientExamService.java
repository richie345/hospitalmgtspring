package com.geemla.hospitalapp.services.interfaces;

import com.geemla.hospitalapp.domains.PatientExam;
import com.geemla.hospitalapp.dto.PatientExamDto;

import java.util.List;

/**
 * @author Emoche
 **/
public interface PatientExamService extends BaseService<PatientExam> {
    PatientExam save(PatientExamDto entryDetailsDto);
    PatientExam update(PatientExamDto entryDetailsDto);
    PatientExam findByPatientId(String patientId);
    PatientExam findByEntryId(String entryId);
    List<PatientExam> findAll();
}
