package com.geemla.hospitalapp.services.interfaces;

import com.geemla.hospitalapp.domains.Specializations;
import com.geemla.hospitalapp.dto.SpecializationDto;

import java.util.List;

/**
 * @author Emoche
 **/
public interface SpecializationsService extends BaseService<Specializations> {
    Specializations save(SpecializationDto vitalSignsDto);
    Specializations findByIDD(String idd);
    List<Specializations> findAll();
}
