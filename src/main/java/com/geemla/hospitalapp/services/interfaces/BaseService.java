package com.geemla.hospitalapp.services.interfaces;

import com.geemla.hospitalapp.domains.base.Model;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface BaseService<T extends Model> {

    boolean existsById(Long Long);

    boolean deleteModelById(Long id);

    boolean deleteModel(T model);

    Optional<T> findOneById(Long Long);

    T getOne(Long Long);

    Long count();

    List<T> findAll();

    Page<T> findAll(Pageable pageable);

    Optional<T> update(@NotNull(message = "Cannot saveOrUpdate a null entity") T model);

    Optional<T> save(@NotNull(message = "Cannot saveOrUpdate a null entity") T model);

    T saveModel(@NotNull(message = "Cannot saveOrUpdate a null entity") T model);

    List<T> saveAll(List<T> models) ;

    void deleteAll(List<T> models);

}
