package com.geemla.hospitalapp.services.interfaces;


import com.geemla.hospitalapp.domains.actors.BackofficeUser;
import com.geemla.hospitalapp.dto.actors.BackofficeUserDto;

public interface BackofficeUserService extends BaseService<BackofficeUser> {
    BackofficeUser save(BackofficeUserDto backofficeUserDto);
    BackofficeUser update(BackofficeUserDto backofficeUserDto);
    BackofficeUser findByUuid(String uuid);
    Long countAllActive();
}
