package com.geemla.hospitalapp.services.interfaces;


import com.geemla.hospitalapp.domains.PasswordResetToken;
import com.geemla.hospitalapp.domains.VerificationToken;
import com.geemla.hospitalapp.domains.actors.User;
import com.geemla.hospitalapp.dto.actors.UserDto;
import com.geemla.hospitalapp.exceptions.UserAlreadyExistException;

import java.util.Collection;
import java.util.Optional;

public interface UserService {

    User createUser(UserDto accountDto) throws UserAlreadyExistException;

    void updateUser(final UserDto accountDto);
    
    void updateLastLogIn(final User user);

    void deleteUser(User user);

    User getUser(final String verificationToken);

    void createVerificationTokenForUser(User user, String token);

    VerificationToken getVerificationToken(String VerificationToken);

    Optional<VerificationToken> generateNewVerificationToken(String token);

    //String validateVerificationToken(RegistrationConfirmationDto requestDto);

    void createPasswordResetTokenForUser(User user, String token);

    PasswordResetToken getPasswordResetToken(String token);

    User getUserByPasswordResetToken(String token);

    User findByUsername(String username);

    User findUserByEmail(String email);

    User findUserByUiid(String uiid);

    User findUserByID(long id);
    
    User findByUsernameOrEmail(String usernameOrEmail);

    //void changeUserPassword(final User user, final ChangePasswordDto changePasswordDto);

    boolean checkIfValidOldPassword(User user, String password);

    Collection<User> findAllUsers();
    
    Optional<User> getUserByUsernameOrEmail(final String usernameOrEmail);
    
    String validatePasswordResetToken(String token, String uuid);
    
    //void saveResetPassword(final ResetPasswordDto resetPasswordDto) ;

    Optional<User> getLoggedInUser();

    User getLoginUserElseThrow();

    //void sendRegistrationEmail(User user);
}
