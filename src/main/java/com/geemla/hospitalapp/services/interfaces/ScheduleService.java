package com.geemla.hospitalapp.services.interfaces;

import com.geemla.hospitalapp.domains.Schedule;
import com.geemla.hospitalapp.dto.ScheduleDto;

import java.util.List;

/**
 * @author Emoche
 **/
public interface ScheduleService extends BaseService<Schedule> {
    Schedule save(ScheduleDto entryDetailsDto);
    Schedule update(ScheduleDto entryDetailsDto);
    Schedule findByPatientId(String patientId);
    List<Schedule> findAllByPatientId(String patientId);
    Schedule findByEntryId(String entryId);
    List<Schedule> findAll();
}
