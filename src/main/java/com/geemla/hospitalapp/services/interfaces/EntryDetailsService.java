package com.geemla.hospitalapp.services.interfaces;

import com.geemla.hospitalapp.domains.EntryDetails;
import com.geemla.hospitalapp.dto.EntryDetailsDto;

import java.util.List;

/**
 * @author Emoche
 **/
public interface EntryDetailsService extends BaseService<EntryDetails> {
    EntryDetails save(EntryDetailsDto entryDetailsDto);
    EntryDetails findByPatientId(String patientId);
    EntryDetails findByEntryId(String entryId);
    List<EntryDetails> findAll();
}
