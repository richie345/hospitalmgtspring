package com.geemla.hospitalapp.services.interfaces;

import com.geemla.hospitalapp.domains.Symptoms;
import com.geemla.hospitalapp.dto.SymptomsDto;

import java.util.List;

/**
 * @author Emoche
 **/
public interface SymptomsService extends BaseService<Symptoms> {
    Symptoms save(SymptomsDto vitalSignsDto);
    Symptoms findByIDD(String idd);
    List<Symptoms> findAll();
}
