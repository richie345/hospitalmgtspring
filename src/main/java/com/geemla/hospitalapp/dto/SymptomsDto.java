package com.geemla.hospitalapp.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Data
public class SymptomsDto {
    private Long id;

    @NotNull
    private String IDD;

    private String Name;
}
