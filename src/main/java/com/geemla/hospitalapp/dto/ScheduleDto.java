package com.geemla.hospitalapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.geemla.hospitalapp.constants.PatientType;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Data
public class ScheduleDto {
    private Long id;

    @NotNull
    private String patientId;

    private String entryId;

    private String date_of_schedule;

    private String time_for_schedule;

    private String reason_of_schedule;

    private String doctor_assigned;

    private String appointment_status_id;
    private String schedule_type_id;
    private String registeredBy;

    @Enumerated(EnumType.STRING)
    private PatientType patientType;

    private String appointment_status_note;
    private String email_remindal;
    private String phone_remindal;
    private String schedule_status;
    private String refferal_detail;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private String dateRegistered;
}
