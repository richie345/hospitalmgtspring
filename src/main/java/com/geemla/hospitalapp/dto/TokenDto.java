package com.geemla.hospitalapp.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class TokenDto {
	
	@NotNull
    private String token;
}
