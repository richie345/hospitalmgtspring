package com.geemla.hospitalapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.geemla.hospitalapp.domains.actors.User;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * @author Emoche
 **/

@Setter
@Getter
public class AuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private final String uuid;
    private final String username;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final Set<String> roles;
    private final Set<String> permissions;
    private final String token;
    private final Long expiresIn;
    
    @JsonFormat
    (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private final Date lastLoggedIn;
    
    @JsonFormat
    (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private final Date createdAt;
    
    public AuthenticationResponse(String token, Long expiresIn, User user) {
    	this.token = token;
        this.uuid = user.getUuid();
        this.username = user.getUsername();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.roles = (Set<String>) user.getRoleNames();
        this.permissions = (Set<String>) user.getPermissionNames();
        this.lastLoggedIn = user.getLastLoggedIn();
        this.createdAt = user.getCreatedAt();
        this.expiresIn = expiresIn;
    }
}
