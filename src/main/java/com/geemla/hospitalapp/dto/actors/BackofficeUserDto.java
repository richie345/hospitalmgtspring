package com.geemla.hospitalapp.dto.actors;

import com.geemla.hospitalapp.constants.BackOfficeType;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class BackofficeUserDto extends UserDto {
    private Long id, userId;
    private String uuid, userUuid;

    @Enumerated(EnumType.STRING)
    private BackOfficeType backOfficeType;

    private boolean active;
}
