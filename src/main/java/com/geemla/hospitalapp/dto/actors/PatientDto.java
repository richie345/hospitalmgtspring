package com.geemla.hospitalapp.dto.actors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.geemla.hospitalapp.constants.Gender;
import com.geemla.hospitalapp.constants.PatientType;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Data
public class PatientDto {
    private Long id;

    @NotNull
    private String patientId;

    private String titleId;

    @NotNull
    private String surname;

    @NotNull
    private String firstname;

    private String othername;

    @Column(unique = true)
    private String email;

    private String phone;
    private String address;
    private String dob;
    private String bloodGroup;
    private String genotype;
    private String fullNamesKin;
    private String kinPhone;
    private String kinAddress;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private String dateRegistered;

    private String registeredBy;
    private String deathdate;
    private String hospitalId;
    private String patientStatus;
    private String pictureUrl;

    @Enumerated(EnumType.STRING)
    private PatientType patientType;

    private String occupation;
    private String mstatus;
    private String nospouse;
    private String nationality;
    private String religion;
    private String pob;
    private String refferedBy;

    @Enumerated(EnumType.STRING)
    private Gender gender;


    private boolean enabled;

    private boolean deleted;
}
