package com.geemla.hospitalapp.dto.actors;

import com.geemla.hospitalapp.utils.ValidEmail;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author Emoche
 **/

@Data
public class UserDto {
    private Long id;
    private String uuid;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    @ValidEmail
    private String email;
    
    private boolean enabled;

    private List<String> roleNames;
}
