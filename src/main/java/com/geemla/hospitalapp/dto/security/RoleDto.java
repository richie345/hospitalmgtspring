package com.geemla.hospitalapp.dto.security;

import com.geemla.hospitalapp.constants.RoleType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Setter
@Getter
public class RoleDto {
    private Long id;
    private String uuid;

    @NotNull
    @NotBlank
    private String name;

    @NotNull
    private String description;

    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    private boolean active;
}
