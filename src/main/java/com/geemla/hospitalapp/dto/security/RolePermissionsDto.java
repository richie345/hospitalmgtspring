package com.geemla.hospitalapp.dto.security;

import lombok.Data;

import java.util.Collection;

@Data
public class RolePermissionsDto {
    private Long id;
    private String uuid, name;

    private Collection<String> selectedPermissions;
}
