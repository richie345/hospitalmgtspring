package com.geemla.hospitalapp.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Data
public class DiagnosisDto {
    private Long id;

    @NotNull
    private String IDD;

    private String Name;

    private String accuracy;

    private String lcd;

    private String lcdName;

    private String profName;

    private String ranking;
}
