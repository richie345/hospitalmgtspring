package com.geemla.hospitalapp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Data
public class PatientExamDto {
    private Long id;

    @NotNull
    private String patientId;

    private String title;
    private String entryId;

    @NotNull
    private String presentingComplain;

    private String historyOfPresentIllness;

    private String medications;

    private String allergies;

    private String pastMedicalHistory;
    private String familyHistory;
    private String socialHistory;
    private String drugHistory;
    private String occupationalHistory;
    private String reviewOfSystems;
    private String physicalMentalExamination;
    private String lab;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private String createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private String updatedAt;

    private String assessment;
    private String carriedOutBy;
    private String dateCarriedOut;
    private String remarks;
    private String valid;
}
