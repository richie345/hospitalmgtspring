package com.geemla.hospitalapp.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Data
public class EntryDetailsDto {
    private Long id;

    @NotNull
    private String patientId;

    private String entryType;
    private String entryId;

    private String registeredBy;
}
