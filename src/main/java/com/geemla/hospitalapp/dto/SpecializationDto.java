package com.geemla.hospitalapp.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Data
public class SpecializationDto {
    private Long id;

    @NotNull
    private String IDD;

    private String Name;

    private String specialistID;

    private String issueID;
}
