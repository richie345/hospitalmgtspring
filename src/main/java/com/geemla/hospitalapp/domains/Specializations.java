package com.geemla.hospitalapp.domains;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.geemla.hospitalapp.domains.base.Model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Entity
@Table(name = "specializations")
@Getter
@Setter
public class Specializations extends Model {
    public Specializations(){

    }

    @NotNull
    private String IDD;

    private String Name;

    private String specialistID;

    private String issueID;

}
