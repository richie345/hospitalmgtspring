package com.geemla.hospitalapp.domains.base;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
@ToString(callSuper = true)
public abstract class Model extends Audit<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    private String uuid;

//    protected boolean active;

    public Model() {}

    @PreUpdate
    public void setUpdateAt() {
        setUpdatedAt(new Date());
        this.preUpdate();
    }

    @PrePersist
    public void setCreatedAt() {
        if (this.getCreatedAt() == null) {
            this.setUpdatedAt(new Date());
            this.setCreatedAt(new Date());
        }
        uuid = UUID.randomUUID().toString();
        this.prePersist();
    }
}
