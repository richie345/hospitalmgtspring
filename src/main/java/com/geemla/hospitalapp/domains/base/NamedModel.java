package com.geemla.hospitalapp.domains.base;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@MappedSuperclass
public abstract class NamedModel extends Model{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1683746778327859773L;
	
	@Column(unique = true, nullable = false)
	@NotNull(message = "Name Cannot be null")
    @Size(min=3)
    private String name;
}
