package com.geemla.hospitalapp.domains;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.geemla.hospitalapp.constants.Gender;
import com.geemla.hospitalapp.constants.PatientType;
import com.geemla.hospitalapp.domains.base.Model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Entity
@Table(name = "patientexam")
@Getter
@Setter
public class PatientExam extends Model {

  public PatientExam(){

  }

  @NotNull
  private String patientId;

  private String title;
  private String entryId;

  @NotNull
  private String presentingComplain;

  private String historyOfPresentIllness;

  private String medications;

  private String allergies;

  private String pastMedicalHistory;
  private String familyHistory;
  private String socialHistory;
  private String drugHistory;
  private String occupationalHistory;
  private String reviewOfSystems;
  private String physicalMentalExamination;
  private String lab;


  private String assessment;
  private String carriedOutBy;
  private String dateCarriedOut;
  private String remarks;
  private String valid;


}
