package com.geemla.hospitalapp.domains.actors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.geemla.hospitalapp.domains.base.Model;
import com.geemla.hospitalapp.security.Permission;
import com.geemla.hospitalapp.security.Role;
import com.geemla.hospitalapp.utils.WebUtility;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Emoche
 **/

@Entity
@Table(name = "users")
@Getter
@Setter
public class User extends Model {

    public static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    public User(){

    }

    public User(final String lastName, final String firstName, final String email, final String password, final boolean enabled) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.password = password;
        this.enabled = enabled;
    }

    @Column(name = "username", nullable = false, unique = true)
    private String username;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @NotNull
    private String lastName;

    @NotNull
    private String firstName;

    @Column(unique = true)
    private String email;

    private String phoneNumber;

    private boolean enabled;

    private boolean deleted;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    @JsonFormat
    (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Date lastLoggedIn;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    @JsonFormat
    (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd hh:mm:ss")
    private Date lastPasswordResetDate;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles;

    public String getNames() {
        return lastName + " " + firstName;
    }

    @JsonIgnore
    public Set<Permission> getPermissions() {

        if (roles == null) {
            roles = new HashSet<>();
        }
        return roles.stream().map(role ->
                role.getPermissions().stream().map(perm -> perm).collect(Collectors.toSet())
        ).flatMap(Collection::stream).collect(Collectors.toSet());
    }

    public Collection<String> getRoleNames() {
        if (roles == null) {
            roles = new HashSet<>();
        }

        return roles.stream().map(p -> p.getName()).collect(Collectors.toSet());
    }

    public String getFirstRoleName() {
        if (roles == null) {
            roles = new HashSet<>();
        }
        Optional<Role> r = roles.stream().findFirst();
        if (r.isPresent()) {
            return r.get().getName();
        }
        return null;
    }

    public Role getFirstRole() {
        if (roles == null) {
            roles = new HashSet<>();
        }
        Optional<Role> r = roles.stream().findFirst();
        if (r.isPresent()) {
            return r.get();
        }
        return null;
    }
    
    public Collection<String> getPermissionNames() {
        if (roles == null) {
            roles = new HashSet<>();
        }

        return roles.stream().map(role ->
		        role.getPermissions().stream().map(perm -> perm.getName()).collect(Collectors.toSet())
		).flatMap(Collection::stream).collect(Collectors.toSet());
    }

    @PrePersist
    void createdAt() {
        super.setCreatedAt();
        this.lastPasswordResetDate = this.lastLoggedIn = new Date();
        this.deleted = false;
        if (StringUtils.isEmpty(this.username)) {
            this.username = this.email;
        }

        if (!WebUtility.isBcryptHashed(this.password)) {
            this.setPassword(PASSWORD_ENCODER.encode(this.password));
        }
    }

    @PreUpdate
    void updatedAt() {
        super.setUpdateAt();

        if (!WebUtility.isBcryptHashed(this.password)) {
            this.setPassword(PASSWORD_ENCODER.encode(this.password));
        }
    }
}
