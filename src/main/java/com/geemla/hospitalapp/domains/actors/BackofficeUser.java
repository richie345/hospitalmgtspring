package com.geemla.hospitalapp.domains.actors;

import com.geemla.hospitalapp.constants.BackOfficeType;
import com.geemla.hospitalapp.domains.base.Model;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "backoffice_users")
@Data
public class BackofficeUser extends Model {

    @Enumerated(EnumType.STRING)
    private BackOfficeType backOfficeType;

    @OneToOne(targetEntity = User.class, cascade = {CascadeType.ALL})
    private User user;

}
