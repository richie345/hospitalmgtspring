package com.geemla.hospitalapp.domains.actors;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.geemla.hospitalapp.constants.Gender;
import com.geemla.hospitalapp.constants.PatientType;
import com.geemla.hospitalapp.domains.base.Model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/

@Entity
@Table(name = "patient")
@Getter
@Setter
public class Patient extends Model {

    public Patient(){

    }

    @NotNull
    private String patientId;

    private String titleId;

    @NotNull
    private String surname;

    @NotNull
    private String firstname;

    private String othername;

    @Column(unique = true)
    private String email;

    private String phone;
    private String address;
    private String dob;
    private String bloodGroup;
    private String genotype;
    private String fullNamesKin;
    private String kinPhone;
    private String kinAddress;

    private String registeredBy;
    private String deathdate;
    private String hospitalId;
    private String patientStatus;
    private String pictureUrl;

    @Enumerated(EnumType.STRING)
    private PatientType patientType;

    private String occupation;
    private String mstatus;
    private String nospouse;
    private String nationality;
    private String religion;
    private String pob;
    private String refferedBy;

    @Enumerated(EnumType.STRING)
    private Gender gender;


    private boolean enabled;

    private boolean deleted;

}
