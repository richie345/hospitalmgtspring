package com.geemla.hospitalapp.domains;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.geemla.hospitalapp.constants.Gender;
import com.geemla.hospitalapp.constants.PatientType;
import com.geemla.hospitalapp.domains.base.Model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Entity
@Table(name = "vitalsigns")
@Getter
@Setter
public class VitalSigns extends Model {

    public VitalSigns(){

    }

    @NotNull
    private String patientId;

    private String entryId;

    private String patientAge;

    private String heartRateMinutes;

    private String temperature;

    private String systolicBloodPressure;

    private String diastolicBloodPressure;
    private String respiratoryRate;
    private String registeredBy;


}
