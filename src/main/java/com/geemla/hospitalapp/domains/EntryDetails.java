package com.geemla.hospitalapp.domains;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.geemla.hospitalapp.domains.base.Model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Entity
@Table(name = "entrydetails")
@Getter
@Setter
public class EntryDetails extends Model {

    public EntryDetails(){

    }

    @NotNull
    private String patientId;

    private String entryType;
    private String entryId;

    private String registeredBy;

}
