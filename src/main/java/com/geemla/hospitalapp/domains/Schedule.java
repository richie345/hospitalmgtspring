package com.geemla.hospitalapp.domains;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.geemla.hospitalapp.constants.PatientType;
import com.geemla.hospitalapp.domains.base.Model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Entity
@Table(name = "schedule")
@Getter
@Setter
public class Schedule extends Model {
    public Schedule(){

    }

    @NotNull
    private String patientId;

    private String entryId;

    private String date_of_schedule;

    private String time_for_schedule;

    private String reason_of_schedule;

    private String doctor_assigned;

    private String appointment_status_id;
    private String schedule_type_id;
    private String registeredBy;

    @Enumerated(EnumType.STRING)
    private PatientType patientType;

    private String appointment_status_note;
    private String email_remindal;
    private String phone_remindal;
    private String schedule_status;
    private String refferal_detail;


}
