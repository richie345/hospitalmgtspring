package com.geemla.hospitalapp.domains;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.geemla.hospitalapp.domains.base.Model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Emoche
 **/
@Entity
@Table(name = "diagnosis")
@Getter
@Setter
public class Diagnosis extends Model {
    public Diagnosis(){

    }

    @NotNull
    private String IDD;

    private String Name;

    private String accuracy;

    private String lcd;

    private String lcdName;

    private String profName;

    private String ranking;

}
