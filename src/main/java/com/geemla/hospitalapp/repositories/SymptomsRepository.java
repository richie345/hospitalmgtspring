package com.geemla.hospitalapp.repositories;

import com.geemla.hospitalapp.domains.Symptoms;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Emoche
 **/
public interface SymptomsRepository extends JpaRepository<Symptoms, Long> {
    Symptoms findByIDD(String paramString);

    List<Symptoms> findAll();
}
