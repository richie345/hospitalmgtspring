package com.geemla.hospitalapp.repositories.security;

import com.geemla.hospitalapp.constants.RoleType;
import com.geemla.hospitalapp.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Emoche
 **/

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);

    Role findByUuid(String uuid);

    List<Role> findAllByRoleTypeAndActive(RoleType roleType, boolean active);

    boolean existsByName(String name);
}
