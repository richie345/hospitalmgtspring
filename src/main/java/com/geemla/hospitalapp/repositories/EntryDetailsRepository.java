package com.geemla.hospitalapp.repositories;

import com.geemla.hospitalapp.domains.EntryDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Emoche
 **/
public interface EntryDetailsRepository extends JpaRepository<EntryDetails, Long> {
    EntryDetails findByPatientId(String paramString);
    EntryDetails findByEntryId(String paramString);

    List<EntryDetails> findAll();
}
