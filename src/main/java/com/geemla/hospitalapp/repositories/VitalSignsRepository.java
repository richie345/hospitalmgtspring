package com.geemla.hospitalapp.repositories;

import com.geemla.hospitalapp.domains.VitalSigns;
import com.geemla.hospitalapp.domains.actors.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Emoche
 **/
public interface VitalSignsRepository extends JpaRepository<VitalSigns, Long> {
    VitalSigns findByUuid(String paramString);

    List<VitalSigns> findByPatientId(Patient patient);
}
