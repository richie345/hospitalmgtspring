package com.geemla.hospitalapp.repositories;

import com.geemla.hospitalapp.domains.Diagnosis;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Emoche
 **/
public interface DiagnosisRepository extends JpaRepository<Diagnosis, Long> {
    Diagnosis findByIDD(String paramString);

    List<Diagnosis> findAll();
}
