package com.geemla.hospitalapp.repositories;

import com.geemla.hospitalapp.domains.Specializations;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Emoche
 **/
public interface SpecializationsRepository extends JpaRepository<Specializations, Long> {

    Specializations findByIDD(String paramString);

    List<Specializations> findAll();
}
