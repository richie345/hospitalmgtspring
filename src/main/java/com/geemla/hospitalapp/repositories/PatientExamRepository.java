package com.geemla.hospitalapp.repositories;

import com.geemla.hospitalapp.domains.PatientExam;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Emoche
 **/
public interface PatientExamRepository extends JpaRepository<PatientExam, Long> {
    PatientExam findByPatientId(String paramString);
    PatientExam findByEntryId(String paramString);

    List<PatientExam> findAll();
}
