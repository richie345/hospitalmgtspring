package com.geemla.hospitalapp.repositories.actors;

import com.geemla.hospitalapp.constants.BackOfficeType;
import com.geemla.hospitalapp.domains.actors.BackofficeUser;
import com.geemla.hospitalapp.domains.actors.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BackofficeUserRepository extends JpaRepository<BackofficeUser, Long> {
    BackofficeUser findByUuid(String uuid);
    BackofficeUser findByUser(User user);
    List<BackofficeUser> findAllByBackOfficeType(BackOfficeType backOfficeType);
    Long countAllByActive(boolean active);
}
