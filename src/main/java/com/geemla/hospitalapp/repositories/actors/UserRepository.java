package com.geemla.hospitalapp.repositories.actors;

import com.geemla.hospitalapp.domains.actors.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Emoche
 **/

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findByUuid(String uuid);

    User findByEmail(String email);

}
