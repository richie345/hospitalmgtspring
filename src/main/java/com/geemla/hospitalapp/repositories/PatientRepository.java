package com.geemla.hospitalapp.repositories;

import com.geemla.hospitalapp.domains.actors.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Emoche
 **/
public interface PatientRepository extends JpaRepository<Patient, Long> {
    Patient findByUuid(String paramString);

    List<Patient> findAllByActive(boolean paramBoolean);
}
