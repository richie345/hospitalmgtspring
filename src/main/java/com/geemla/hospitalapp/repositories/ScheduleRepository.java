package com.geemla.hospitalapp.repositories;

import com.geemla.hospitalapp.domains.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Emoche
 **/
public interface ScheduleRepository extends JpaRepository<Schedule, Long> {
    Schedule findByPatientId(String paramString);
    Schedule findByEntryId(String paramString);
    List<Schedule> findAllByPatientId(String patientId);

    List<Schedule> findAll();
}
