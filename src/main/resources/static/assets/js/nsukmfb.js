function searchArrayObjectById(idKey, objArray){
    for (var i=0; i < objArray.length; i++) {
        if (objArray[i].id == idKey) {
            return objArray[i];
        }
    }
}

$(function(){
    $.extend({
        showToast: function(obj) {
            var toastHtml = '<div aria-live="polite" aria-atomic="true" style="position: relative; min-height: 200px;">';
            toastHtml += '<div class="toast" style="position: absolute; top: 0; right: 0;">';
            toastHtml += '<div class="toast-header">';
            toastHtml += obj.icon;
            toastHtml += '<strong class="mr-auto">' + obj.title + '</strong>';
            toastHtml += '<small>' + obj.mutedText + '</small>';
            toastHtml += '<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">';
            toastHtml += '<span aria-hidden="true">&times;</span></button></div>';
            toastHtml += '<div class="toast-body">' + obj.content + '</div>';
            toastHtml += '</div></div>';

            $('div#appToasts').append(toastHtml);
            $('.toast').toast('show');
        }
    });
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function serializeFormData(formData) {
    // var object = {};
    // formData.forEach((value, key) => {
    //     // Reflect.has in favor of: object.hasOwnProperty(key)
    //     if(!Reflect.has(object, key)){
    //         object[key] = value;
    //         return;
    //     }
    //     if(!Array.isArray(object[key])){
    //         object[key] = [object[key]];
    //     }
    //     object[key].push(value);
    // });
    // return  JSON.stringify(object);

    var object = {};
    formData.forEach(function(value, key){
        object[key] = value;
    });
    return JSON.stringify(object);
}

function serializeFormArray(formArray) {
    var object = {};
    $.each(formArray, function( index, item ){
        object[item.name] = item.value;
    });
    return JSON.stringify(object);
}
